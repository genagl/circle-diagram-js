import { PELandedEntity } from "@common/entities";
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from "@nestjs/swagger";
import { UserEntity } from "@users/entities";
import { Column, Entity, ManyToOne } from "typeorm";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";

@Entity("fmru_track")
@ObjectType("PEFestTrack", { description: 'pe-fest-tracks' })
export class PEFestTrackEntity extends PELandedEntity {
    
    @Column({ nullable: false, default: false })
    @Field({  description: 'Is track closed for join?', defaultValue: false })
    @ApiProperty({example: false, required: false, description: 'Is track closed for join?' })
    is_closed?: boolean; 

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Track moderator's ID" })
    @Field(() => Int, { nullable: true } )
    moderator_id?: number; 
     
    @Field(() => UserEntity, { description: "Track moderator" }) 
    moderator?: UserEntity; 

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Track moderator assistant's ID" })
    @Field(() => Int, { nullable: true } )
    moderator2_id?: number;
    
    @Field(() => UserEntity, { description: "Track moderator assistant" }) 
    moderator2?: UserEntity;
    
    @Column("varchar", {nullable: true, default: "" })  
    @Field(() => String, { description: "Youtube describe's ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    video_id?: string;
    
    @Column("varchar", {nullable: true })  
    @Field(() => String, { description: "illustration", nullable: true }) 
    @ApiProperty({ nullable: true  })
    illustration?: string;
    

    @ManyToOne(() => PEFestProjectEntity, (project) => project.track) 
    @Field({  description: 'Child Project list', nullable: 'items' })
    project?: PEFestProjectEntity[];
    
}