import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestTrackDto } from "./dto/create-pe-fest-track.dto";
import { PEFestTrackEntity } from "./entities/pe-fest-track.entity";
import { PEFestTrackService } from "./pe-fest-track.service";
import { UserEntity } from "@users/entities";
import { UseInterceptors } from "@nestjs/common";
import { UpdateURLAvatarInterceptor } from "@users/interceptors";


@Resolver(() => PEFestTrackEntity)
export class PEFestTrackResolver extends PEResolver(
    PEFestTrackEntity,
    CreatePEFestTrackDto, 
    "PEFestTrack"
) {
    constructor( 
        private readonly _service: PEFestTrackService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
    
    @ResolveField("moderator", () => UserEntity, { complexity: 4, nullable: true, description: "Track moderator" })
    @UseInterceptors(UpdateURLAvatarInterceptor)
    async moderator(@Parent() track:  PEFestTrackEntity ) {
        const moderator_id: number = track.moderator_id; 
        return moderator_id 
            ? 
            this._userRepository.findById( moderator_id )
            : 
            null; 
    }
    
    @ResolveField("moderator2", () => UserEntity, { complexity: 4, nullable: true, description: "Track moderator assistant" })
    @UseInterceptors(UpdateURLAvatarInterceptor)
    async moderator2(@Parent() track:  PEFestTrackEntity ) {
        const moderator_id: number = track.moderator2_id; 
        return moderator_id 
            ? 
            this._userRepository.findById( moderator_id )
            : 
            null; 
    }
}