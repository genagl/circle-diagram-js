import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestTrackService } from "./pe-fest-track.service";

@Controller('pefesttracks') 
@ApiTags("pefesttracks") 
export class PEFestTrackController extends PEController {
    constructor( private readonly _peFestTrackService: PEFestTrackService ){
        super( _peFestTrackService );
    }        
}