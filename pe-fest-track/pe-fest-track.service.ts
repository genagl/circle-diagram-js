import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestTrackEntity } from "./entities/pe-fest-track.entity";
import { CreatePEFestTrackDto } from "./dto/create-pe-fest-track.dto";

@Injectable()
export class PEFestTrackService extends PERepository<PEFestTrackEntity, CreatePEFestTrackDto> {
    constructor( 
        @InjectRepository(PEFestTrackEntity)
        private readonly _peFestTrackRepository: Repository<PEFestTrackEntity>,
    ) {
        super(_peFestTrackRepository, "pe-fest-track"); 
    }
}