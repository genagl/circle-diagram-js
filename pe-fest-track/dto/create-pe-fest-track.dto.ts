import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestTrackInput")
export class CreatePEFestTrackDto extends CreatePEDto {
    
}