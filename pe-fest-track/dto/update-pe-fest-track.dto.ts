import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestTrackDto } from './create-pe-fest-track.dto';

@InputType("PEFestTrackInput")
export class UpdatePEFestTrackDto extends PartialType(CreatePEFestTrackDto) {

}