import { PEFestTrackService } from './pe-fest-track.service';
import { PEFestTrackResolver } from './pe-fest-track.resolver';
import { PEFestTrackController } from './pe-fest-track.controller';
import { PEFestTrackEntity } from './entities/pe-fest-track.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestTrackEntity ]),
        LandsModule,
        UsersModule], 
    providers: [PEFestTrackService, PEFestTrackResolver,], 
    controllers: [ PEFestTrackController, ],
    exports: [PEFestTrackService]
})
export class PEFestTrackModule {}
