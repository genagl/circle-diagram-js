import { registerEnumType } from "@nestjs/graphql";

export enum CARD_TYPE {
    BOX             = "BOX",
    ONLY_LINE       = "ONLY_LINE",
    SIPLE_STROKE    = "SIPLE_STROKE",
    STROKE          = "STROKE",
    CARD            = "CARD",
    TWO_SIZE_CARD   = "TWO_SIZE_CARD"
}

registerEnumType(CARD_TYPE, {
   name: 'CARD_TYPE',
});