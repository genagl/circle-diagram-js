import { registerEnumType } from "@nestjs/graphql";

export enum RATING_TYPES {
    INDEPENDENT_GRADE       = "INDEPENDENT_GRADE",
    DISTRIBUTION_PROCENTAGE = "DISTRIBUTION_PROCENTAGE",
    PROCENTAGE              = "PROCENTAGE" 
}

registerEnumType(RATING_TYPES, {
    name: 'RATING_TYPES',
});
 
export enum FESTIVAL_RATING_TYPES {
    INDEPENDENT_GRADE       = "INDEPENDENT_GRADE", 
    PROCENTAGE              = "PROCENTAGE",
    EACH_CATEGORY           = "EACH_CATEGORY"
} 

registerEnumType(FESTIVAL_RATING_TYPES, {
    name: 'FESTIVAL_RATING_TYPES',
});