import { registerEnumType } from "@nestjs/graphql";

export enum DISPLAY_PROJECT_RESULT {
    AVERAGE="AVERAGE",
    SUMMAE="SUMMAE",
    SUMMAE_MILESTONES="SUMMAE_MILESTONES"
}

registerEnumType(DISPLAY_PROJECT_RESULT, {
    name: 'DISPLAY_PROJECT_RESULT',
});
