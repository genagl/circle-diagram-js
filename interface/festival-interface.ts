import { HasId, Role } from "@common/interface"; 
import { CARD_TYPE, DISPLAY_PROJECT_RESULT, FESTIVAL_RATING_TYPES } from "./";
import { ILand } from "@lands/interface";
import { PEFestivalExtendedFieldEntity } from "../pe-fest-extended-fields/entities/pe-festival-extended-field.entity";

export interface IFestivalBase{
    refreshRate?: number;
    maxMemberId?: number;
    roles: Role[];
    domain_type_id: string;
    memberPerPage?: number;
    horoRaiting?: number;
    enabledRules?: boolean;
    isIndependentGradeCritery?: boolean;
    isOwnerRate?: boolean;
    isGanres?: boolean;
    maxGanres?: number;
    isRegisterProject?: boolean;
    isCreateAuthorWithProject?: boolean;
    isProjectsVerify?: boolean;
    isExpertsCriteries?: boolean;
    isShowMap?: boolean;
    isShowReglament?: boolean;
    isShowLaureates?: boolean;
    isShowTracks?: boolean;
    isChangeCardType?: boolean;
    isDiary?: boolean;
    maxRating?: number;
    ratingType?: FESTIVAL_RATING_TYPES; 
    displayProjectResult?: DISPLAY_PROJECT_RESULT;
    memberCardType?: CARD_TYPE;
    memberCardHeight?: number;
    defaultProjectThumbnail?: string;
    newProjectEmail?: string;
    newProjectMessage?: string;
    extendedFields?: PEFestivalExtendedFieldEntity[];
}

export type IFestival = IFestivalBase & HasId & ILand;