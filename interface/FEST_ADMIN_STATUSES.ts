import { registerEnumType } from "@nestjs/graphql";

export enum FEST_ADMIN_STATUSES {
    BEFORE_START_PHASE  = 0, 
    PREPAIRE_PHASE      = 1, 
    PRESENTATION_PHASE  = 2, 
    AFTER_FINISH_PHASE  = 3
}

registerEnumType(FEST_ADMIN_STATUSES, {
    name: 'FEST_ADMIN_STATUSES',
});