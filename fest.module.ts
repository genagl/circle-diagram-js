 
import { LandsModule } from '@lands/lands.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { PEFestivalController,  PEFestivalResolver, PEFestivalService, } from './';
import { PEFestivalEntity } from './entities';
import { PeFestCorrectRatingModule } from './pefest-correct-rating/pefest-correct-rating.module';
import { PeFestRatingModule } from './pefest-rating/pefest-rating.module';
import { PeFestExtendedFieldModule } from './pe-fest-extended-fields/pe-fest-extended-fields.module';
import { PEFestProjectModule } from './pe-fest-project/pe-fest-project.module'; 
import { PEfestCategoryModule } from './pefest-category/pefest-category.module';
import { PeFestDestrictModule } from './pe-fest-destrict/pe-fest-destrict.module';
import { PEFestCriteryModule } from './pe-fest-critery/pe-fest-critery.module';
import { PEFestMilestoneModule } from './pe-fest-milestone/pe-fest-milestone.module';
import { PEFestTrackModule } from './pe-fest-track/pe-fest-track.module';
import { PEFestGanreModule } from './pe-fest-ganre/pe-fest-ganre.module';
import { PEFestDiaryModule } from './pe-fest-diary/pe-fest-diary.module';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestivalEntity, ]),
        LandsModule,
        UsersModule,
        PeFestCorrectRatingModule,
        PeFestRatingModule,
        PeFestExtendedFieldModule,
        PEFestProjectModule,
        PEfestCategoryModule, 
        PeFestDestrictModule,
        PEFestCriteryModule,
        PEFestMilestoneModule,
        PEFestTrackModule,
        PEFestGanreModule,
        PEFestDiaryModule
    ], 
    exports: [ PEFestivalService],
    providers: [ PEFestivalService, PEFestivalResolver, ], 
    controllers: [ PEFestivalController, ] 
})
export class FestModule {}
