import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestivalService } from "./pe-festival.service";

@Controller('pefestivals') 
@ApiTags("pefestivals") 
export class PEFestivalController extends PEController {
    constructor( private readonly _pefestivalService: PEFestivalService ){
        super( _pefestivalService );
    }        
}