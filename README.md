# Fest module

Protopia-Ecosystem NestJS module

<p align="center">
  <img src="https://gitlab.com/uploads/-/system/project/avatar/57094677/pe_FEST_large_logo.png" width="100" alt="Ludens Logo" />
</p> 

Модуль для [ProtopiaEcosystem NodeJS клиента](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/). 
Включает в себя набор ресурсов и API-методов (GraphQL и REST) для развертывания и сопровождения онлайн и оффлайн образовательных фестивалей.

![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)

## Подробное описание:  

Модуль позволяет:

- регистрировать проекты участникам Фестиваля. В команду Проекта входят: Автор, Наставник и до 10-ти участников
- оценивать эти проекты Экспертам
- организовывать самооценивание Проектов и взаимное оценивание по системе "360" в рамках организуемых "на лету" флэш-сообществ (Соты)
- гибко структурировать Фестиваль, дополняя его Треками, настраиваемой системой оценки, подразделяя Проекты на территориальные (региональные) группы, группы по Жанрам, группы по образовательным учреждениям.
- Ведение дневника Проекта и инструменты координации участников Проекта.
И много другое. Подробности - в документации.

- Гибкое администрирование Фестиваля, включая подключение сторонних расширений (PE-модулей), увеличивающих его функциональность
- Микширование Фестиваля с другими типами PE-событий ( смю PE-модуль «PE-Tопос»)
