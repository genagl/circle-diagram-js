import PagingDto from '@common/dto/paging.dto';
import { ID } from '@common/interface';
import { Injectable } from '@nestjs/common';
import { PEFestAttachmentService } from '../pe-fest-attachment/pe-fest-attachment.service';
import { PEFestMilestoneService } from '../pe-fest-milestone/pe-fest-milestone.service';
import { PeFestRatingService } from '../pefest-rating/pefest-rating.service';
import { PEFestProjectService } from './pe-fest-project.service';
import { createProjectMilestoneUtil } from './utils';
import { PEFestCriteryService } from '../pe-fest-critery/pe-fest-critery.service';

@Injectable()
export class PeFestProjectMilestoneService {

  constructor(
    private readonly _milestone: PEFestMilestoneService,
    private readonly _project: PEFestProjectService,
    private readonly _rating: PeFestRatingService,
    private readonly _attachment: PEFestAttachmentService,
    private readonly _critery: PEFestCriteryService,

  ){

  } 

  async findAll(projectId: ID, land_id: ID) : Promise<any[]> { 
    /*  
      TODO: избыточно, пофиксить!!!!! 
      Всё это перенесено с АПИ Wordpress в этом безобразном виде!!!! 
      Никогда ТАК не делай!!!!!!
      Тебе ещё и клиент отрефакторить!!!!
    */
    // получаем тип оценивания данного Фестиваля
    
    //
    const criteries = await this._critery.findAll({}, Number(land_id) );
    // собираем оценки проекта projectId
    const findObj = { 
      metas: [
        {
          key:"member_id", value_numeric: Number(projectId), 
        },
      ] 
    } as PagingDto; 
    const ratings = await this._rating.findFilter(findObj, parseInt(land_id.toString()));

    //Собираем Вложения (PEFestAttachment) Проекта
    const find2 = { 
      metas: [
        {
          key:"member_id", value_numeric: Number(projectId), 
        },
      ] 
    } as PagingDto;
    const attachments = await this._attachment.findFilter(find2, parseInt(land_id.toString()));

    // Собираем все Этапы и, если есть оценки для Проекта/Этапа, раскидываем их
    const milestoneObjs = await this._milestone.findAll({}, parseInt(land_id.toString()) );  
    
    const milestones = milestoneObjs.map(milestone => {
      const crits = criteries.filter(c => c.milestone_id === milestone.id).map(c => c.id);
      const ratings1 = ratings.filter(r => crits.includes(r.criteryId) );
      return createProjectMilestoneUtil(
        milestone,
        ratings1,
        attachments,
        projectId,
        land_id
      )
    });
    
    return milestones || [];
  }
}
