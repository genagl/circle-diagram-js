import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestProjectDto } from "./dto/create-pefestproject.dto";
import { PEFestProjectEntity } from "./entities/pe-fest-project.entity";
import { PEFestProjectService } from "./pe-fest-project.service";
import { UserEntity } from "@users/entities";
import { PEFestTrackEntity } from "../pe-fest-track/entities/pe-fest-track.entity";
import { PEFestTrackService } from "../pe-fest-track/pe-fest-track.service";
import { PEFestHoneycombsEntity } from "../pe-fest-honeycombs/entities/pe-fest-honeycombs.entity";
import { PEFestHoneycombsService } from "../pe-fest-honeycombs/pe-fest-honeycombs.service";
import { PEFestDestrictEntity } from "../pe-fest-destrict/entities/pe-fest-destrict.entity";
import { PEFestDestrictService } from "../pe-fest-destrict/pe-fest-destrict.service";
import { PeFestRatingService } from "../pefest-rating/pefest-rating.service";
import PagingDto from "@common/dto/paging.dto";


@Resolver(() => PEFestProjectEntity)
export class PEFestProjectResolver extends PEResolver(
    PEFestProjectEntity,
    CreatePEFestProjectDto, 
    "PEFestProject"
) {
    constructor( 
        private readonly _service: PEFestProjectService,
        private readonly _track: PEFestTrackService,
        private readonly _honeycombs: PEFestHoneycombsService,
        private readonly _fmru_destrict: PEFestDestrictService,
        private readonly _rating: PeFestRatingService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    } 

    @ResolveField("track", () => PEFestTrackEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async track(@Parent() project:  PEFestProjectEntity ) {
        const track_id: number = project.track_id;  
        return track_id 
            ? 
            this._track.findById( track_id )
            : 
            null;
    }

    @ResolveField("honeycombs", () => PEFestHoneycombsEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async honeycombs(@Parent() project:  PEFestProjectEntity ) {
        const honeycombs_id: number = project.honeycombs_id;  
        return honeycombs_id 
            ? 
            this._honeycombs.findById( honeycombs_id )
            : 
            null;
    }

    @ResolveField("fmru_destrict", () => PEFestDestrictEntity, { complexity: 4, nullable: true, description: "Parent Destrict." })
    async fmru_destrict(@Parent() project:  PEFestProjectEntity ) {
        const fmru_destrict_id: number = project.fmru_destrict_id;  
        return fmru_destrict_id 
            ? 
            this._fmru_destrict.findById( fmru_destrict_id )
            : 
            null;
    } 

    @ResolveField("ratings", () => PEFestDestrictEntity, { complexity: 4, nullable: true, description: "Ratings" })
    async ratings(@Parent() project:  PEFestProjectEntity ) {
        return this._rating.findFilter( 
            {
                metas: [
                    {
                        key: "member_id",
                        value_numeric: project.id
                    }
                ]
            } as PagingDto,
            Number(project.land_id) 
        );
    }

    @ResolveField("tutor", () => UserEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async tutor(@Parent() project:  PEFestProjectEntity ) {
        const tutor_id: number = project.tutor_id;  
        return tutor_id 
            ? 
            this._userRepository.findById( tutor_id )
            : 
            null;
    }

    @ResolveField("leader", () => UserEntity, { complexity: 4, nullable: true, description: "Leader." })
    async leader(@Parent() project:  PEFestProjectEntity ) {
        const leader_id: number = project.leader_id;  
        return leader_id 
            ? 
            this._userRepository.findById( leader_id )
            : 
            null; 
    }
    
    @ResolveField("member0", () => UserEntity, { complexity: 4, nullable: true, description: "member 0." })
    async member0(@Parent() project:  PEFestProjectEntity ) {
        const member0_id: number = project.member0_id;  
        return member0_id 
            ? 
            this._userRepository.findById( member0_id )
            : 
            null; 
    }
    
    @ResolveField("member1", () => UserEntity, { complexity: 4, nullable: true, description: "member 1." })
    async member1(@Parent() project:  PEFestProjectEntity ) {
        const member1_id: number = project.member1_id;  
        return member1_id 
            ? 
            this._userRepository.findById( member1_id )
            : 
            null; 
    }
    
    @ResolveField("member2", () => UserEntity, { complexity: 4, nullable: true, description: "member 2." })
    async member2(@Parent() project:  PEFestProjectEntity ) {
        const member2_id: number = project.member2_id;  
        return member2_id 
            ? 
            this._userRepository.findById( member2_id )
            : 
            null; 
    }
    
    @ResolveField("member3", () => UserEntity, { complexity: 4, nullable: true, description: "member 3." })
    async member3(@Parent() project:  PEFestProjectEntity ) {
        const member3_id: number = project.member3_id;  
        return member3_id 
            ? 
            this._userRepository.findById( member3_id )
            : 
            null; 
    }
    
    @ResolveField("member4", () => UserEntity, { complexity: 4, nullable: true, description: "member 4." })
    async member4(@Parent() project:  PEFestProjectEntity ) {
        const member4_id: number = project.member4_id;  
        return member4_id 
            ? 
            this._userRepository.findById( member4_id )
            : 
            null; 
    }
    
    @ResolveField("member5", () => UserEntity, { complexity: 4, nullable: true, description: "member 5." })
    async member5(@Parent() project:  PEFestProjectEntity ) {
        const member5_id: number = project.member5_id;  
        return member5_id 
            ? 
            this._userRepository.findById( member5_id )
            : 
            null; 
    }
    
    @ResolveField("member6", () => UserEntity, { complexity: 4, nullable: true, description: "member 6." })
    async member6(@Parent() project:  PEFestProjectEntity ) {
        const member6_id: number = project.member6_id;  
        return member6_id 
            ? 
            this._userRepository.findById( member6_id )
            : 
            null; 
    }
    
    @ResolveField("member7", () => UserEntity, { complexity: 4, nullable: true, description: "member 7." })
    async member7(@Parent() project:  PEFestProjectEntity ) {
        const member7_id: number = project.member7_id;  
        return member7_id 
            ? 
            this._userRepository.findById( member7_id )
            : 
            null; 
    }
    
    @ResolveField("member8", () => UserEntity, { complexity: 4, nullable: true, description: "member 8." })
    async member8(@Parent() project:  PEFestProjectEntity ) {
        const member8_id: number = project.member8_id;  
        return member8_id 
            ? 
            this._userRepository.findById( member8_id )
            : 
            null; 
    }
 
}