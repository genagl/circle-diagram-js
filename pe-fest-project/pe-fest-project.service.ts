import { Injectable } from "@nestjs/common";  
import { Repository, SelectQueryBuilder } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestProjectEntity } from "./entities/pe-fest-project.entity";
import { CreatePEFestProjectDto } from "./dto/create-pefestproject.dto";
import { IPaging } from "@common/interface";

@Injectable()
export class PEFestProjectService extends PERepository<PEFestProjectEntity, CreatePEFestProjectDto> {
    constructor( 
        @InjectRepository(PEFestProjectEntity)
        private readonly _pefestprojectRepository: Repository<PEFestProjectEntity>,
    ) {
        super(_pefestprojectRepository, "pefestproject"); 
    } 
}