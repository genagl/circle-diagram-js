import { ID } from '@common/interface';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { PEFestProjectMilestoneEntity } from './entities/pe-fest-project-milestone.entity';
import { PeFestProjectMilestoneService } from './pe-fest-project-milestone.service';

@Resolver(() => PEFestProjectMilestoneEntity)
export class PeFestProjectMilestoneResolver {
  constructor(private readonly _service: PeFestProjectMilestoneService) {}

  @Query(() => [PEFestProjectMilestoneEntity], { name: 'getPEFestProjectMilestones', nullable: false, defaultValue:[] })
  findAll(
    @Args('id', { type: () => String, description: "Project id" }) id: ID,
    @Args('land_id', { type: () => String }) land_id: ID
  ) {
    return this._service.findAll(id, land_id);
  }
  
}
