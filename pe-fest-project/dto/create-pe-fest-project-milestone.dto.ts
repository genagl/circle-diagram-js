import { Field, InputType } from "@nestjs/graphql";
import { CreatePEFestAttachmentDto } from "../../pe-fest-attachment/dto/create-pe-fest-attachment.dto";

@InputType("PEFestProjectMilestoneInput")
export class CreatePeFestProjectMilestoneDto {

    @Field({ nullable: true, description: 'Average calculate raring of Project in Milestone'  })
    public averageRating: string;

    @Field(() => [CreatePEFestAttachmentDto], { nullable: true, description: 'List of Attachments', })
    public attachments: CreatePEFestAttachmentDto[];
}