import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestProjectDto } from './create-pefestproject.dto';

@InputType("PEFestProjectInput")
export class UpdatePEFestProjectDto extends PartialType(CreatePEFestProjectDto) {

}