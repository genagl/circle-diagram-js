import { Field, Float, InputType, Int } from "@nestjs/graphql"; 
import { ApiProperty } from "@nestjs/swagger";
import { CreatePEDto } from "@src/common/dto/create-pe.dto";
import GraphQLJSON from "graphql-type-json"; 

@InputType("PEFestProjectInput")
export class CreatePEFestProjectDto extends CreatePEDto {
    @ApiProperty({ required: false, description: 'Parent PEFestDestrict ID' })
    @Field(() => Int, { nullable: true } )
    fmru_destrict_id?: number;

    @ApiProperty({ required: false, description: 'Parent PEFestTrack ID' })
    @Field(() => Int, { nullable: true } )
    track_id?: number;

    @ApiProperty({ required: false, description: 'Parent PEFestHoneycombs ID' })
    @Field(() => Int, { nullable: true } )
    honeycombs_id?: number;

    @ApiProperty({  required: false, description: 'Tutor ID' })
    @Field(() => Int, { nullable: true } )
    tutor_id?: number;

    @ApiProperty({ required: false, description: 'Leader ID' })
    @Field(() => Int, { nullable: true } )
    leader_id?: number;

    @ApiProperty({ required: false, description: 'Member 0 ID' })
    @Field(() => Int, { nullable: true } )
    member0_id?: number;

    @ApiProperty({ required: false, description: 'Member 1 ID' })
    @Field(() => Int, { nullable: true } )
    member1_id?: number;

    @ApiProperty({ required: false, description: 'Member 2 ID' })
    @Field(() => Int, { nullable: true } )
    member2_id?: number;

    @ApiProperty({ required: false, description: 'Member 3 ID' })
    @Field(() => Int, { nullable: true } )
    member3_id?: number;

    @ApiProperty({ required: false, description: 'Member 4 ID' })
    @Field(() => Int, { nullable: true } )
    member4_id?: number;

    @ApiProperty({ required: false, description: 'Member 5 ID' })
    @Field(() => Int, { nullable: true } )
    member5_id?: number;

    @ApiProperty({ required: false, description: 'Member 6 ID' })
    @Field(() => Int, { nullable: true } )
    member6_id?: number;

    @ApiProperty({ required: false, description: 'Member 7 ID' })
    @Field(() => Int, { nullable: true } )
    member7_id?: number;

    @ApiProperty({ required: false, description: 'Member 8 ID' })
    @Field(() => Int, { nullable: true } )
    member8_id?: number;

    @ApiProperty({ required: false, description: 'Member 9 ID' })
    @Field(() => Int, { nullable: true } )
    member9_id?: number;

    @Field(() => String, {  
        description: "Nomination",  
        nullable: true 
    })
    nomination?: string;

    @Field(() => String, {  
        description: "Prize",  
        nullable: true 
    })
    prize?: string;

    @Field(() => Boolean, {  
        description: "Is project verified by Track moderator?",  
        nullable: true 
    })
    is_verified?: boolean;

    @Field(() => [[GraphQLJSON]], {  
        description: "Extended fields, created by Festival's matrix", 
        nullable: true 
    })
    form_fields?: any[][];

    @Field(() => Float, {  
        description: "Actual rating",  
        nullable: true 
    })
    actualRating?: number; 
}