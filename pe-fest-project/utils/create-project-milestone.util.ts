import { ID } from "@common/interface";
import { PEFestAttachmentEntity } from "@modules/fest/pe-fest-attachment/entities/pe-fest-attachment.entity";
import { PEFestMilestoneEntity } from "@modules/fest/pe-fest-milestone/entities/pe-fest-milestone.entity";
import { PEFestRatingEntity } from "@modules/fest/pefest-rating/entities/pefest-rating.entity";
import { RATED_OBJECT } from "@modules/fest/pefest-rating/interface";
import { countAverageRating } from "@modules/fest/pefest-rating/utils";

export const createProjectMilestoneUtil = (
    milestone: PEFestMilestoneEntity,
    ratings: PEFestRatingEntity[],
    attachments: PEFestAttachmentEntity[],
    projectId: ID,
    land_id: ID
) => {
    // ВЫСЧИСЛЯЕИ СРЕДНИЙ РЕЙТИНГ 
    const averageRating: string = countAverageRating(
        RATED_OBJECT.PROJECT_MILESTONE,  
        ratings || [], 
        projectId, 
        land_id, 
        milestone.id,
        -1
      );

      return {  
        ...milestone,
        milestone,
        ratings,
        attachments: attachments.filter(a => a.milestone_id === milestone.id),
        experts: ratings.map(r => ({id: r.expertId, display_name: r.display_name, })),
        descriptions: ratings.map(r => r.description),
        averageRating,
        count: ratings.length,
      };
}