import { LandEntity } from "@lands/entities";
import { Field, Int, ObjectType } from "@nestjs/graphql";
import { UserEntity } from "@users/entities";
import { Column, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { PEFestAttachmentEntity } from "../../pe-fest-attachment/entities/pe-fest-attachment.entity";
import { PEFestMilestoneEntity } from "../../pe-fest-milestone/entities/pe-fest-milestone.entity";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";
import { PEFestRatingEntity } from "../../pefest-rating/entities/pefest-rating.entity";

@ObjectType("PEFestProjectCritery", {description: "Data of Project in single Critery."})
export class PEFestProjectMilestoneEntity {

    @PrimaryGeneratedColumn()
    @Field( () => Int ) 
    public id: number; 
    
    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */
    @ManyToOne( () => LandEntity, {
        orphanedRowAction: "soft-delete",
        cascade: true
    } ) 
    @JoinColumn({ name: "land_id" })
    @Field( () => LandEntity, { nullable: true, description: 'Land' } ) 
    public land_id: LandEntity;

    @Column({ nullable: true })
    @Field({ nullable: true, description: 'Average calculate raring of Project in Milestone'  })
    public averageRating: string;
    
    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */
    @ManyToOne( () => PEFestMilestoneEntity, {
        orphanedRowAction: "soft-delete",
        cascade: true
    } ) 
    @JoinColumn({ name: "milestone_id" })
    @Field(() => PEFestMilestoneEntity, { nullable: true, description: 'Milestone' })
    public milestone: PEFestMilestoneEntity;
 
    /* https://orkhan.gitbook.io/typeorm/docs/many-to-many-relations */
    @ManyToOne( () => PEFestProjectEntity, {
        orphanedRowAction: "soft-delete",
        cascade: true
    } ) 
    @JoinColumn({ name: "project_id" })
    @Field(() => PEFestProjectEntity, { nullable: true, description: 'Project' })
    public member: PEFestProjectEntity;
 
    @Field(() => [PEFestRatingEntity], { nullable: true, description: 'List of Ratings', })
    public ratings : PEFestRatingEntity[];
 
    @Field(() => [PEFestAttachmentEntity], { nullable: true, description: 'List of Attachments', })
    public attachments: PEFestAttachmentEntity[];

    @Field(() => [UserEntity], { nullable: true, description: 'List of rated experts', })
    public experts: UserEntity[];

    @Field(() => [String], { nullable: true, description: 'List of Experts descriptions', })
    public descriptions: string[];
}