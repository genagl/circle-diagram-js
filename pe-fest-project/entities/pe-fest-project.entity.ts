import { PELandedEntity } from "@common/entities";
import { Field, Float, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from "@nestjs/swagger";
import { UserEntity } from "@users/entities";
import GraphQLJSON from "graphql-type-json";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, RelationId, VersionColumn } from "typeorm";
import { PEFestDestrictEntity } from "../../pe-fest-destrict/entities/pe-fest-destrict.entity";
import { PEFestDiaryEntity } from "../../pe-fest-diary/entities/pe-fest-diary.entity";
import { PEFestExhortationEntity } from "../../pe-fest-exhortation/entities/pe-fest-exhortation.entity";
import { PEFestGanreEntity } from "../../pe-fest-ganre/entities/pe-fest-ganre.entity";
import { PEFestHoneycombsEntity } from "../../pe-fest-honeycombs/entities/pe-fest-honeycombs.entity";
import { PEFestSchoolEntity } from "../../pe-fest-school/entities/pe-fest-school.entity";
import { PEFestTrackEntity } from "../../pe-fest-track/entities/pe-fest-track.entity";
import { PeFestCorrectRatingEntity } from "../../pefest-correct-rating/entities/pefest-correct-rating.entity";
import { PEFestRatingEntity } from "../../pefest-rating/entities/pefest-rating.entity";

@Entity("pe-fest-projects")
@ObjectType("PEFestProject", { description: 'pe-fest-projects' })
export class PEFestProjectEntity extends PELandedEntity {
        
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Parent PEFestDestrict ID' })
    @Field(() => Int, { nullable: true } )
    fmru_destrict_id?: number;
    
    @Field({  description: 'Parent PEFestDestrict', nullable: true })
    fmru_destrict?: PEFestDestrictEntity;
    
    @ManyToMany(() => PEFestSchoolEntity, (school) => school.project)
    @JoinTable()
    @Field({  description: 'Parent PEFestSchools list', nullable: 'itemsAndList' })
    fmru_school?: PEFestSchoolEntity[];
    
    @ManyToMany(() => PEFestGanreEntity, category => category.project)
    @JoinColumn()
    @Field({  description: 'Parent PEFestGanre list', nullable: 'items' })
    ganre?: PEFestGanreEntity[];
        
    @Column({ name: "fmru_track", nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Parent PEFestTrack ID' })
    @Field(() => Int, {nullable: true } )
    track_id?: number;

    @Field({  description: 'Parent PEFestTrack', nullable: true })
    track?: PEFestTrackEntity;
     
    @Column({name: "fmru_honeycombs",  nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Parent PEFestHoneycombs ID' })
    @Field(() => Int, { nullable: true } )
    honeycombs_id?: number;

    @Field({  description: 'Parent PEFestHoneycombs', nullable: true })
    honeycombs?: PEFestHoneycombsEntity;

    @Field(() => UserEntity, { description: 'Tutor' }) 
    tutor?: UserEntity;  

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Tutor ID' })
    @Field(() => Int, { nullable: true } )
    tutor_id?: number;
     
    @Field(() => UserEntity, { description: 'Leader' }) 
    leader?: UserEntity; 
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Leader ID' })
    @Field(() => Int, { nullable: true } )
    leader_id?: number;
 
    @Field(() => UserEntity, { description: 'member 0' }) 
    member0?: UserEntity;  
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 0 ID' })
    @Field(() => Int, { nullable: true } )
    member0_id?: number;
     
    @Field(() => UserEntity, { description: 'member 1' }) 
    member1?: UserEntity;   
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 1 ID' })
    @Field(() => Int, { nullable: true } )
    member1_id?: number;
    
    @Field(() => UserEntity, { description: 'member 2' }) 
    member2?: UserEntity;   
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 2 ID' })
    @Field(() => Int, { nullable: true } )
    member2_id?: number;
     
    @Field(() => UserEntity, { description: 'member 3' }) 
    member3?: UserEntity;   
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 3 ID' })
    @Field(() => Int, { nullable: true } )
    member3_id?: number;
     
    @Field(() => UserEntity, { description: 'member 4' }) 
    member4?: UserEntity;   
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 4 ID' })
    @Field(() => Int, { nullable: true } )
    member4_id?: number;
    
    @Field(() => UserEntity, { description: 'member 5' }) 
    member5?: UserEntity;  
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 5 ID' })
    @Field(() => Int, { nullable: true } )
    member5_id?: number; 
     
    @Field(() => UserEntity, { description: 'member 6' }) 
    member6?: UserEntity;    
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 6 ID' })
    @Field(() => Int, { nullable: true } )
    member6_id?: number;
     
    @Field(() => UserEntity, { description: 'member 7' }) 
    member7?: UserEntity;     
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 7 ID' })
    @Field(() => Int, { nullable: true } )
    member7_id?: number;

    @Field(() => UserEntity, { description: 'member 8' }) 
    member8?: UserEntity;      
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 8 ID' })
    @Field(() => Int, { nullable: true } )
    member8_id?: number;

    @Field(() => UserEntity, { description: 'member 9' }) 
    member9?: UserEntity;  
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'member 9 ID' })
    @Field(() => Int, { nullable: true } )
    member9_id?: number;   
    
    @Field(() => [UserEntity], { description: 'Experts, who rated.' }) 
    experts?: UserEntity[];  

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "Notification",  
        nullable: true 
    })
    notification?: string;

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "Nomination",  
        nullable: true 
    })
    nomination?: string;

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "Prize",  
        nullable: true 
    })
    prize?: string;

    @Column("boolean", {nullable: true})
    @Field(() => Boolean, {  
        description: "Is project verified by Track moderator?",  
        nullable: true 
    })
    is_verified?: boolean;

    @Column( "simple-array", { nullable: true })
    @Field(() => [[GraphQLJSON]], {  
        description: "Extended fields, created by Festival's matrix",  
        defaultValue: [],
        nullable: true 
    })
    form_fields?: any[][];
 
    @VersionColumn({nullable: true, default: 0})
    @Field(() => Int, {  
        description: "Version",  
        nullable: true  
    })
    version?: number;
 
    @Column("float", {nullable: true, default: 0})
    @Field(() => String, {  
        description: "Actual rating",  
        nullable: true 
    })
    actualRating?: number;

    @Field(() => [PeFestCorrectRatingEntity], {nullable: true, description: "Correct rating by Track moderator"})
    @OneToMany(() => PeFestCorrectRatingEntity, (file:PeFestCorrectRatingEntity) => file.project )
    @ApiProperty({ type: PeFestCorrectRatingEntity, isArray:true, example: [], nullable: true })
    correctMemberRate?: PeFestCorrectRatingEntity[];

    @Field(() => [PEFestRatingEntity], {nullable: true, description: "Ratings list for Project"})
    @OneToMany(() => PEFestRatingEntity, (file:PEFestRatingEntity) => file.project )
    @ApiProperty({ type: PEFestRatingEntity, isArray:true, example: [], nullable: true })
    ratings?: PEFestRatingEntity[];

    @Field(() => [PEFestExhortationEntity], {nullable: true, description: "Exhortations list for Project"})
    @OneToMany(() => PEFestExhortationEntity, (file:PEFestExhortationEntity) => file.project )
    @ApiProperty({ type: PEFestExhortationEntity, isArray:true, example: [], nullable: true })
    exhortations?: PEFestExhortationEntity[];

    @Field(() => [PEFestDiaryEntity], {nullable: true, description: "Dairies list by Project's members"})
    @OneToMany(() => PEFestDiaryEntity, (diary:PEFestDiaryEntity) => diary.project )
    @ApiProperty({ type: PEFestDiaryEntity, isArray:true, example: [], nullable: true })
    diaries?: PEFestDiaryEntity[];
 
}