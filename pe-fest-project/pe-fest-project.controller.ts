import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestProjectService } from "./pe-fest-project.service";

@Controller('pefestprojects') 
@ApiTags("pefestprojects") 
export class PEFestProjectController extends PEController {
    constructor( private readonly _pefestprojectService: PEFestProjectService ){
        super( _pefestprojectService );
    }        
}