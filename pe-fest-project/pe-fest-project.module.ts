import { LandsModule } from '@lands/lands.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { PEFestAttachmentModule } from '../pe-fest-attachment/pe-fest-attachment.module';
import { PeFestDestrictModule } from '../pe-fest-destrict/pe-fest-destrict.module';
import { PeFestExhortationModule } from '../pe-fest-exhortation/pe-fest-exhortation.module';
import { PeFestExtendedFieldModule } from '../pe-fest-extended-fields/pe-fest-extended-fields.module';
import { PEFestGanreModule } from '../pe-fest-ganre/pe-fest-ganre.module';
import { PEFestHoneycombsModule } from '../pe-fest-honeycombs/pe-fest-honeycombs.module';
import { PEFestMilestoneModule } from '../pe-fest-milestone/pe-fest-milestone.module';
import { PEFestTrackModule } from '../pe-fest-track/pe-fest-track.module';
import { PeFestCorrectRatingModule } from '../pefest-correct-rating/pefest-correct-rating.module';
import { PeFestRatingModule } from '../pefest-rating/pefest-rating.module';
import { PEFestProjectMilestoneEntity } from './entities/pe-fest-project-milestone.entity';
import { PEFestProjectEntity } from './entities/pe-fest-project.entity';
import { PeFestProjectMilestoneResolver } from './pe-fest-project-milestone.resolver';
import { PeFestProjectMilestoneService } from './pe-fest-project-milestone.service';
import { PEFestProjectController } from './pe-fest-project.controller';
import { PEFestProjectResolver } from './pe-fest-project.resolver';
import { PEFestProjectService } from './pe-fest-project.service';
import { PEFestCriteryModule } from '../pe-fest-critery/pe-fest-critery.module';
 

@Module({
    imports: [
        TypeOrmModule.forFeature([ PEFestProjectEntity, PEFestProjectMilestoneEntity ]),
        LandsModule,
        UsersModule,
        PeFestExtendedFieldModule,
        PeFestRatingModule,
        PeFestCorrectRatingModule,
        PeFestDestrictModule,
        PeFestExhortationModule,
        PEFestTrackModule,
        PEFestHoneycombsModule,
        PEFestMilestoneModule,
        PEFestGanreModule,
        PEFestAttachmentModule,
        PEFestCriteryModule
    ],
    providers: [PEFestProjectService, PEFestProjectResolver, PeFestProjectMilestoneService, PeFestProjectMilestoneResolver],
    controllers: [ PEFestProjectController ],
    exports: [PEFestProjectService, PeFestProjectMilestoneService],
})
export class PEFestProjectModule {}
