import { InputType, Int, Field } from '@nestjs/graphql';

@InputType("PEFestRatingInput")
export class CreatePefestRatingInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
