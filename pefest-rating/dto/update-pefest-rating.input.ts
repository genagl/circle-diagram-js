import { CreatePefestRatingInput } from './create-pefest-rating.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType( )
export class UpdatePefestRatingInput extends PartialType(CreatePefestRatingInput) {
  @Field(() => Int)
  id: number;
}
