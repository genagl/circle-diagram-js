import { Resolver, Query, Mutation, Args, Int, ID } from '@nestjs/graphql';
import { PeFestRatingService } from './pefest-rating.service'; 
import { CreatePefestRatingInput } from './dto/create-pefest-rating.input'; 
import { PEFestRatingEntity } from '../pefest-rating/entities/pefest-rating.entity';

@Resolver(() => PEFestRatingEntity)
export class PeFestRatingResolver {
  constructor(
    private readonly service: PeFestRatingService
  ) {}

  @Mutation(() => PEFestRatingEntity, { name: "createPEFestRating" })
  createPefestRating( @Args('input') createPefestRatingInput: CreatePefestRatingInput) {
    return this.service.create(createPefestRatingInput);
  }

  @Query(() => [PEFestRatingEntity], { name: 'getPEFestRatings' })
  findAll( @Args("land_id", { type: () => ID }) land_id: number | string ) {
    return this.service.findAll( {}, Number(land_id) );
  }

  @Query(() => PEFestRatingEntity, { name: 'getPEFestRating' })
  findOne(@Args('id', { type: () => Int }) id: number ) {
    return this.service.findOne(id);
  }

  @Mutation(() => PEFestRatingEntity, { name: "changePEFestRating" })
  updatePefestRating(@Args('id') id: number, @Args('input') input: CreatePefestRatingInput) {
    return this.service.update( id, input);
  }

  @Mutation(() => PEFestRatingEntity, { name: "deletePEFestRating" })
  removePefestRating(@Args('id', { type: () => Int }) id: number) {
    return this.service.remove(id);
  }
}
