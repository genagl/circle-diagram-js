import { PEFestRatingEntity } from "../entities/pefest-rating.entity";

export const averageCritery = (ratings: PEFestRatingEntity[]): string => {
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    let average: number = 0;
    ratings.forEach(r => {
        average += parseFloat(r.rating);
    })
    return ( average / (ratings.length || 1) ).toFixed(2);
}