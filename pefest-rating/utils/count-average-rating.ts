import { ID } from "@common/interface";
import { RATED_OBJECT } from "../interface";
import { PEFestRatingEntity } from "../entities/pefest-rating.entity";
import { averageCategory } from "./count-average-category";
import { averageCritery } from "./count-average-critery";
import { averageMilestone } from "./count-average-milestone";
import { averageProject } from "./count-average-project";

export const countAverageRating = ( 
    src: RATED_OBJECT,  
    ratings: PEFestRatingEntity[], 
    projectId: ID,
    land_id: ID, 
    milestone_id: ID,
    category_id: ID,
): string => { 
    switch(src) {
        case RATED_OBJECT.PROJECT_CRITERY:
            return averageCritery(ratings);
        case RATED_OBJECT.PROJECT_CATEGORY:
            return averageCategory(ratings, category_id, land_id);
        case RATED_OBJECT.PROJECT_MILESTONE:
            return averageMilestone(ratings, milestone_id, land_id);
        case RATED_OBJECT.PROJECT:
        default:
            return averageProject(ratings, projectId, land_id);
    }
}