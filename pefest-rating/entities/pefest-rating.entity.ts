import { LandEntity } from '@lands/entities';
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { PEFestProjectEntity } from '../../pe-fest-project/entities/pe-fest-project.entity';

@Entity("pe-fest-rating")
@ObjectType("PEFestRating")
export class PEFestRatingEntity {
   
  @PrimaryGeneratedColumn()
  @Field(type => ID, { nullable: true,  description: "Uniq identifier." })
  id?: number;
 
  @Column("varchar", { nullable: true })  
  @Field(() => Int, { description: "Rate correct value", nullable: true }) 
  @ApiProperty({ nullable: true  })
  rating?: string;

  @Column("varchar", { nullable: true })  
  @Field(() => String, { description: "Description", nullable: true }) 
  @ApiProperty({ nullable: true  })
  description?: string; 

  @Column({ nullable: true}) 
  @ApiProperty({ example: "", required: false, description: 'expert ID' })
  @Field(() => Int, { nullable: true } )
  expertId?: number;
  
  // @Field(() => UserEntity, { description: 'expert' }) 
  // expert?: UserEntity;  

  @Column("varchar", { nullable: true })  
  @Field(() => String, { description: "display name", nullable: true }) 
  @ApiProperty({ nullable: true  })
  display_name?: string; 
 
  @Column({ nullable: true}) 
  @ApiProperty({ example: "", required: false, description: 'Critery ID' })
  @Field(() => Int, { nullable: true } )
  criteryId?: number;
  
  // @Field(() => PEFestCriteryEntity, { description: 'Critery' })
  // critery?: PEFestCriteryEntity;  

  @Column({ nullable: true }) 
  @Field({  nullable: true, description: "Date" })  
  @ApiProperty({ name: "Date"}) 
  unixtime?: Date | null;
    
  @Column({ name: "member_id", nullable: true }) 
  @Field(() => Int, { nullable: true } )
  memberId?: number; 

  @Field(() => PEFestProjectEntity ) 
  project: PEFestProjectEntity;
 
  @Column("integer",{ nullable: true, default: 0 }) 
  @Field(() => ID, { nullable: true } )
  @ApiProperty({ name: "Land ID"})
  land_id?: number | string;

  @Field(() => LandEntity, { description: 'Land position of Resource' }) 
  land?: LandEntity;
}
