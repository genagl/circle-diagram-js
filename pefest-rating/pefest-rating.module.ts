import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PEFestRatingEntity } from './entities/pefest-rating.entity';
import { PeFestRatingResolver } from './pefest-rating.resolver';
import { PeFestRatingService } from './pefest-rating.service'; 

@Module({
  imports: [TypeOrmModule.forFeature([ PEFestRatingEntity ])],
  providers: [PeFestRatingResolver, PeFestRatingService], 
  exports: [PeFestRatingService], 
})
export class PeFestRatingModule {}
