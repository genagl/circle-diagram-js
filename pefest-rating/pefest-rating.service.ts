import { Injectable } from '@nestjs/common';
import { CreatePefestRatingInput } from './dto/create-pefest-rating.input'; 
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { PEFestRatingEntity } from './entities/pefest-rating.entity';
import { IPaging, ORDER } from '@common/interface';

@Injectable()
export class PeFestRatingService { 
  constructor(
    @InjectRepository(PEFestRatingEntity)
    private repository: Repository<PEFestRatingEntity>,
  ) {}

  _type = 'pe-fest-rating';

  async create(createPefestRatingInput: CreatePefestRatingInput) {
    return 'This action adds a new pefestRating';
  }

  async findAll( filter: IPaging = {}, land_id : number ): Promise<PEFestRatingEntity[]> {
     
    return await this.findFilter(filter, land_id);
  }
  async findFilter( filter: IPaging = {}, land_id?: number ): Promise<any[]> {
    const qb = this.repository.createQueryBuilder(this._type);
    
    if( filter.offset ) {
        qb.offset( filter.offset );
    }

    if( filter.count && filter.count >= 0) {
        qb.limit( filter.count );
    }
    
    let where : any[] = [], 
        order : any = {
            [ `${this._type}.id` ]:     ORDER.DESC, 
            [ `${this._type}.title` ]:  ORDER.DESC, 
        },
        search: string; 

    if( land_id ) {
        where.push( land_id );
        qb.where( `land_id = :land_id`, {land_id} ); 
    }
    if( filter.order ) { 
        order[ `${this._type}.id` ]       = filter.order;
        order[ `${this._type}.title` ]    = filter.order;
    }  

    /*
        meta fields
    */
    if( filter.metas && Array.isArray(filter.metas)) { 
        // TODO:: only meta_relation = "AND"
        filter.metas.forEach(meta => {
            try { 
              qb.where(`${meta.key} = :value`, {value: meta.value_numeric});                 
            }
            catch( e ) {}
        });
    } 
    
    const many = await this.getMany(qb);
    const elements = many.map(e => ({...e, ID: e.id }));
    return elements;
  }

  async getMany(qb: SelectQueryBuilder<PEFestRatingEntity>, filter: IPaging = {} ) {
      return await qb.getMany();
  }

  async findOne(id: number) {
    return `This action returns a #${id} pefestRating`;
  }

  async update(id: number, updatePefestRatingInput: CreatePefestRatingInput) {
    return `This action updates a #${id} pefestRating`;
  }

  async remove(id: number) {
    return `This action removes a #${id} pefestRating`;
  }
}
