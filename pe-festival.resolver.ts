import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestivalDto } from "./dto/create-pefestival.dto";
import { PEFestivalEntity } from "./entities/pe-festival.entity";
import { PEFestivalService } from "./pe-festival.service"; 
import { PEFestivalExtendedFieldEntity } from "./pe-fest-extended-fields/entities/pe-festival-extended-field.entity";
import { PEFestivalExtendedFieldService } from "./pe-fest-extended-fields/pe-fest-extended-fields.service";


@Resolver(() => PEFestivalEntity)
export class PEFestivalResolver extends PEResolver(
    PEFestivalEntity,
    CreatePEFestivalDto, 
    "PEFestival"
) {
    constructor( 
        private readonly _service: PEFestivalService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
        private readonly _extendFields: PEFestivalExtendedFieldService,
    ) {
        super( _service, _lands, _users ); 
    }

    @ResolveField(() => [PEFestivalExtendedFieldEntity], { complexity: 4, nullable: true, description: "Extended fields matrix of Festival's Projects" })
    async extendedFields(@Parent() festival:  PEFestivalEntity ) {
        return await this._extendFields.findAll({}, festival.land?.id );
    }
}