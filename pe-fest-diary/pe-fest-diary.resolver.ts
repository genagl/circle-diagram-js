import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestDiaryDto } from "./dto/create-pe-fest-diary.dto";
import { PEFestDiaryEntity } from "./entities/pe-fest-diary.entity";
import { PEFestDiaryService } from "./pe-fest-diary.service";
import { PEFestProjectEntity } from "../pe-fest-project/entities/pe-fest-project.entity";
import { PEFestProjectService } from "../pe-fest-project/pe-fest-project.service";


@Resolver(() => PEFestDiaryEntity)
export class PEFestDiaryResolver extends PEResolver(
    PEFestDiaryEntity,
    CreatePEFestDiaryDto, 
    "PEFestDiary"
) {
    constructor( 
        private readonly _service: PEFestDiaryService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
        private readonly _project: PEFestProjectService,
    ) {
        super( _service, _lands, _users ); 
    }

    
    @ResolveField("project", () => PEFestProjectEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async project(@Parent() diary:  PEFestDiaryEntity ) {
        const project_id: number = diary.project_id;  
        return project_id 
            ? 
            this._project.findById( project_id )
            : 
            null; 
    }
}