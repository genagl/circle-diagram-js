import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestDiaryInput")
export class CreatePEFestDiaryDto extends CreatePEDto {
    
}