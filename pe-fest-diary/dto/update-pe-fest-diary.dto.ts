import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestDiaryDto } from './create-pe-fest-diary.dto';

@InputType("PEFestDiaryInput")
export class UpdatePEFestDiaryDto extends PartialType(CreatePEFestDiaryDto) {

}