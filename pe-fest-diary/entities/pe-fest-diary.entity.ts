import { Column, Entity } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity("pe-fest-diaries")
@ObjectType("PEFestDiary", { description: 'pe-fest-diaries' })
export class PEFestDiaryEntity extends PELandedEntity { 
    
    @Column({ nullable: true}) 
    @ApiProperty({ required: false, description: 'Parent Project ID' })
    @Field(() => Int, { nullable: true } )
    project_id?: number;

    @Field(() => PEFestProjectEntity, {description: "Parent Project"} ) 
    project: PEFestProjectEntity;
}