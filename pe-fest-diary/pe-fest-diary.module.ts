import { PEFestDiaryService } from './pe-fest-diary.service';
import { PEFestDiaryResolver } from './pe-fest-diary.resolver';
import { PEFestDiaryController } from './pe-fest-diary.controller';
import { PEFestDiaryEntity } from './entities/pe-fest-diary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 
import { Module } from '@nestjs/common';
import { PEFestProjectModule } from '../pe-fest-project/pe-fest-project.module';

@Module({ 
    imports: [
        TypeOrmModule.forFeature([ PEFestDiaryEntity ]),
        LandsModule,
        UsersModule,
        PEFestProjectModule
    ], 
    providers: [PEFestDiaryService, PEFestDiaryResolver,], 
    controllers: [ PEFestDiaryController, ] 
})
export class PEFestDiaryModule {}
