import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestDiaryService } from "./pe-fest-diary.service";

@Controller('pe-fest-diaries') 
@ApiTags("pe-fest-diaries") 
export class PEFestDiaryController extends PEController {
    constructor( private readonly _peFestDiaryService: PEFestDiaryService ){
        super( _peFestDiaryService );
    }        
}