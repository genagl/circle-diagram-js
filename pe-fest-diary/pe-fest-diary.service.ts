import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestDiaryEntity } from "./entities/pe-fest-diary.entity";
import { CreatePEFestDiaryDto } from "./dto/create-pe-fest-diary.dto";

@Injectable()
export class PEFestDiaryService extends PERepository<PEFestDiaryEntity, CreatePEFestDiaryDto> {
    constructor( 
        @InjectRepository(PEFestDiaryEntity)
        private readonly _peFestDiaryRepository: Repository<PEFestDiaryEntity>,
    ) {
        super(_peFestDiaryRepository, "pe-fest-diary"); 
    }
}