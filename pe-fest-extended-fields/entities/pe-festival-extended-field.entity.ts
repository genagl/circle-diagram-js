import { Column, Entity, ManyToOne } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { PEFestivalEntity } from "../../entities/pe-festival.entity";

@Entity("pe-fest-extended-fields")
@ObjectType("PEFestivalExtendedField", { description: 'pe-festival-extended-fields' })
export class PEFestivalExtendedFieldEntity extends PELandedEntity {
     
    @Column("varchar", {nullable: true })  
    @Field(() => String, { description: "description", nullable: true }) 
    @ApiProperty({ nullable: true  })
    description?: string;
     
    @Column("varchar", {nullable: true })  
    @Field(() => String, { description: "name", nullable: true }) 
    @ApiProperty({ nullable: true  })
    name?: string;
     
    @Column("boolean", {nullable: true })  
    @Field(() => Boolean, { description: "require", nullable: true }) 
    @ApiProperty({ nullable: true  })
    require?: boolean;
     
    @Column("boolean", {nullable: true })  
    @Field(() => Boolean, { description: "hide in Project public page", nullable: true }) 
    @ApiProperty({ nullable: true  })
    hide_in_page?: boolean;
     
    @Column("boolean", {nullable: true })  
    @Field(() => Boolean, { description: "hide in card in Projects list", nullable: true }) 
    @ApiProperty({ nullable: true  })
    hide_in_card?: boolean;
     
    @Column("boolean", {nullable: true })  
    @Field(() => Boolean, { description: "hide in Project's admin description", nullable: true }) 
    @ApiProperty({ nullable: true  })
    hide_in_projects?: boolean;
     
    @Column("varchar", {nullable: true })  
    @Field(() => String, { description: "type", nullable: true }) 
    @ApiProperty({ nullable: true  })
    type?: string;
     
    @Column("varchar", {nullable: true })  
    @Field(() => String, { description: "List of values in group", nullable: true }) 
    @ApiProperty({ nullable: true  })
    values?: string;
     
    @Column("integer", {nullable: true })  
    @Field(() => Int, { description: "Count of fields in group (max - 10)", nullable: true }) 
    @ApiProperty({ nullable: true  })
    count?: number;
     
    @Column("varchar", {array:true, nullable: true })  
    @Field(() => [String], { 
        description: "yandex_form_field", 
        deprecationReason:"Remove in next version",
        nullable: true 
    }) 
    @ApiProperty({ nullable: true  })
    yandex_form_field?: string[];

    @Field(() => PEFestivalEntity )
    @ManyToOne(() => PEFestivalEntity, (festival: PEFestivalEntity) => festival.extendedFields)
    festival: PEFestivalEntity;
}