import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestivalExtendedFieldInput")
export class CreatePEFestivalExtendedFieldDto extends CreatePEDto {
    
}