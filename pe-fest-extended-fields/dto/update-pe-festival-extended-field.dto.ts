import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestivalExtendedFieldDto } from './create-pe-festival-extended-field.dto';

@InputType("PEFestivalExtendedFieldInput")
export class UpdatePEFestivalExtendedFieldDto extends PartialType(CreatePEFestivalExtendedFieldDto) {

}