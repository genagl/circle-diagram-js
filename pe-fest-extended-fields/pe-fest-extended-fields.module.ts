import { PEFestivalExtendedFieldEntity } from './entities/pe-festival-extended-field.entity';
import { PEFestivalExtendedFieldService } from './pe-fest-extended-fields.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PEFestivalExtendedFieldResolver } from './pe-fest-extended-fields.resolver';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module';

@Module({
    imports: [TypeOrmModule.forFeature([ PEFestivalExtendedFieldEntity ]),
        UsersModule,
        LandsModule,
    ],
    providers: [ PEFestivalExtendedFieldResolver, PEFestivalExtendedFieldService ],
    exports: [ PEFestivalExtendedFieldService ],
})
export class PeFestExtendedFieldModule {}
