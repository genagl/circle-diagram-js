import { PERepository } from "@common/repositories";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreatePEFestivalExtendedFieldDto } from "./dto/create-pe-festival-extended-field.dto";
import { PEFestivalExtendedFieldEntity } from "./entities/pe-festival-extended-field.entity";


@Injectable()
export class PEFestivalExtendedFieldService extends PERepository<PEFestivalExtendedFieldEntity, CreatePEFestivalExtendedFieldDto> {
    constructor( 
        @InjectRepository(PEFestivalExtendedFieldEntity)
        private readonly _peFestivalExtendedFieldRepository: Repository<PEFestivalExtendedFieldEntity>,
    ) {
        super(_peFestivalExtendedFieldRepository, "pe-festival-extended-field"); 
    }

}