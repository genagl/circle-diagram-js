import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestivalExtendedFieldDto } from "./dto/create-pe-festival-extended-field.dto";
import { PEFestivalExtendedFieldEntity } from "./entities/pe-festival-extended-field.entity";
import { PEFestivalExtendedFieldService } from "./pe-fest-extended-fields.service";


@Resolver(() => PEFestivalExtendedFieldEntity)
export class PEFestivalExtendedFieldResolver extends PEResolver(
    PEFestivalExtendedFieldEntity,
    CreatePEFestivalExtendedFieldDto, 
    "PEFestivalExtendedField"
) {
    constructor( 
        private readonly _service: PEFestivalExtendedFieldService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}