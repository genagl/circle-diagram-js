import { Injectable } from '@nestjs/common';
import { CreatePefestCorrectRatingInput } from './dto/create-pefest-correct-rating.input';
import { UpdatePefestCorrectRatingInput } from './dto/update-pefest-correct-rating.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PeFestCorrectRatingEntity } from './entities/pefest-correct-rating.entity';

@Injectable()
export class PeFestCorrectRatingService {
  constructor(
    @InjectRepository(PeFestCorrectRatingEntity)
    private repository: Repository<PeFestCorrectRatingEntity>,
  ) {}
  create(createPefestCorrectRatingInput: CreatePefestCorrectRatingInput) {
    return 'This action adds a new pefestCorrectRating';
  }

  findAll() {
    return `This action returns all pefestCorrectRating`;
  }

  findOne(id: number) {
    return `This action returns a #${id} pefestCorrectRating`;
  }

  update(id: number, input: CreatePefestCorrectRatingInput) {
    return `This action updates a #${id} pefestCorrectRating`;
  }

  remove(id: number) {
    return `This action removes a #${id} pefestCorrectRating`;
  }
}
