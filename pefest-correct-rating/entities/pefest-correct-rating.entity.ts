import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from '@users/entities';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { PEFestProjectEntity } from '../../pe-fest-project/entities/pe-fest-project.entity';

@Entity("pe-fest-correct-rating")
@ObjectType("PEFestCorrectRating")
export class PeFestCorrectRatingEntity {

  @PrimaryGeneratedColumn()
  @Field(type => ID, { nullable: true,  description: "Uniq identifier." })
  id: number;

  //rate
  @Column("varchar", { nullable: true })  
  @Field(() => String, { description: "Rate correct value", nullable: true }) 
  @ApiProperty({ nullable: true  })
  rate?: string;

  @Column("varchar", { nullable: true })  
  @Field(() => String, { description: "Description", nullable: true }) 
  @ApiProperty({ nullable: true  })
  description?: string; 

  @Column({ nullable: true}) 
  @ApiProperty({ example: "", required: false, description: 'expert ID' })
  @Field(() => Int, { nullable: true } )
  user?: number;
   

  @Column("varchar", { nullable: true })  
  @Field(() => String, { description: "display name", nullable: true }) 
  @ApiProperty({ nullable: true  })
  display_name?: string;   
 
  @Column({ nullable: true }) 
  @Field({  nullable: true, description: "Date" })  
  @ApiProperty({ name: "Date"}) 
  unixtime?: Date | null;

  
  @Field(() => PEFestProjectEntity )
  @ManyToOne(() => PEFestProjectEntity, (pr: PEFestProjectEntity) => pr.correctMemberRate)
  project: PEFestProjectEntity;
}
