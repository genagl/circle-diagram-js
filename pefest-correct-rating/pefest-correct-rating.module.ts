import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PeFestCorrectRatingEntity } from './entities/pefest-correct-rating.entity';
import { PeFestCorrectRatingResolver } from './pefest-correct-rating.resolver';
import { PeFestCorrectRatingService } from './pefest-correct-rating.service';

@Module({
  imports: [TypeOrmModule.forFeature([ PeFestCorrectRatingEntity ])],
  providers: [PeFestCorrectRatingResolver, PeFestCorrectRatingService],
  exports: [PeFestCorrectRatingService],
})
export class PeFestCorrectRatingModule {}
