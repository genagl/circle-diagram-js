import { CreatePefestCorrectRatingInput } from './create-pefest-correct-rating.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdatePefestCorrectRatingInput extends PartialType(CreatePefestCorrectRatingInput) {
  @Field(() => Int)
  id: number;
}
