import { InputType, Int, Field } from '@nestjs/graphql';

@InputType("PeFestCorrectRatingInput")
export class CreatePefestCorrectRatingInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
