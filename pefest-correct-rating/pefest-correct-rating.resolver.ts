import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { PeFestCorrectRatingService } from './pefest-correct-rating.service';
import { CreatePefestCorrectRatingInput } from './dto/create-pefest-correct-rating.input';
import { UpdatePefestCorrectRatingInput } from './dto/update-pefest-correct-rating.input';
import { PeFestCorrectRatingEntity } from './entities/pefest-correct-rating.entity';

@Resolver(() => PeFestCorrectRatingEntity)
export class PeFestCorrectRatingResolver {
  constructor(private readonly pefestCorrectRatingService: PeFestCorrectRatingService) {}

  @Mutation(() => PeFestCorrectRatingEntity, {name: "createPEFestCorrectRating"})
  createPefestCorrectRating(@Args('input') createPefestCorrectRatingInput: CreatePefestCorrectRatingInput) {
    return this.pefestCorrectRatingService.create(createPefestCorrectRatingInput);
  }

  @Query(() => [PeFestCorrectRatingEntity], { name: 'getPEFestCorrectRating' })
  findAll() {
    return this.pefestCorrectRatingService.findAll();
  }

  @Query(() => PeFestCorrectRatingEntity, { name: 'getPEFestCorrectRatings' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.pefestCorrectRatingService.findOne(id);
  }

  @Mutation(() => PeFestCorrectRatingEntity, {name: "changePEFestCorrectRating"})
  updatePefestCorrectRating( @Args('id') id: number, @Args('input') input: CreatePefestCorrectRatingInput  ) {
    return this.pefestCorrectRatingService.update(id, input);
  }

  @Mutation(() => PeFestCorrectRatingEntity, {name: "deletePEFestCorrectRating"})
  removePefestCorrectRating(@Args('id', { type: () => Int }) id: number) {
    return this.pefestCorrectRatingService.remove(id);
  }
}
