import { LandEntity } from '@lands/entities';
import { LandByEntity } from '@lands/entities/land-by.entity';
import { Field, HideField, ID, Int, ObjectType } from '@nestjs/graphql';
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CARD_TYPE, DISPLAY_PROJECT_RESULT, FESTIVAL_RATING_TYPES, IFestival } from '../interface'; 
import { PEFestivalExtendedFieldEntity } from '../pe-fest-extended-fields/entities/pe-festival-extended-field.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Role } from '@users/models/roles.enum'; 

@Entity("festival")
@ObjectType("PEFestival", { description: 'Event type Festival' })
export class PEFestivalEntity extends LandByEntity implements IFestival {
    
    @PrimaryGeneratedColumn()
    @Field(() => ID, { nullable: true,  description: "Uniq identifier." })
    id: number;

    @Column("integer", {nullable: true, default: 5000})
    @Field(() => Int, { 
        description: "Refresh single Project's timeout",  
        nullable: true 
    })
    refreshRate?: number;

    @Column("integer", {nullable: true, default: 0})
    @Field(() => Int, { 
        deprecationReason: "No used in this version",
        description: "Maximum project ID",  
        nullable: true 
    })
    maxMemberId?: number;
  
    @Field(() => [Role], { 
        description: "List of available roles", 
        nullable: true,
        defaultValue: [],
        deprecationReason: "No use! This field added for backward compatibility with legacy APIs. Will be removed in the next version." 
    }) 
    @ApiProperty({ enum: Role, enumName: 'Role', type: [Role],  example: [Role.User], nullable: false  })
    roles: Role[]

  
    @Field(() => String, { 
        description: "List of available roles", 
        nullable: true,
        defaultValue: "0",
        deprecationReason: "No use! This field added for backward compatibility with legacy APIs. Will be removed in the next version." 
    }) 
    @ApiProperty({ example: "0", nullable: false  })
    domain_type_id: string;

    @Column("integer", {nullable: true, default: 30})
    @Field(() => Int, {
        deprecationReason: "Use global paging.count parameter", 
        description: "Count cards",  
        nullable: true 
    })
    memberPerPage?: number;

    @Column("integer", {nullable: true, default: 0})
    @Field(() => Int, {
        deprecationReason: "No used in this version", 
        description: "Removed parameter",  
        nullable: true 
    })
    horoRaiting?: number;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        deprecationReason: "No used in this version",
        description: "Removed parameter",  
        nullable: true 
    })
    enabledRules?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Is independent grade criteries rate?",  
        nullable: true 
    })
    isIndependentGradeCritery?: boolean;

    @Column("boolean", {nullable: true, default: false})
    @Field(() => Boolean, {  
        description: "Is owner of Project may rate himself?",  
        nullable: true 
    })
    isOwnerRate?: boolean;

    @Column("boolean", {nullable: true, default: false})
    @Field(() => Boolean, {   
        description: "Is Ganres enabled in Festival?",  
        nullable: true 
    })
    isGanres?: boolean;

    @Column("integer", {nullable: true, default: 3})
    @Field(() => Int, {   
        description: "Maximum ganres on Project",  
        nullable: true 
    })
    maxGanres?: number;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {   
        description: "May register new Projects without event organizers?",  
        nullable: true 
    })
    isRegisterProject?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "During the registration process of a new project, is a new User automatically created as a Tutor for this Project?",  
        nullable: true 
    })
    isCreateAuthorWithProject?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {   
        description: "The new project is marked as unverified and rights are limited until verification",  
        nullable: true 
    })
    isProjectsVerify?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {   
        description: "Experts may create new criteries",  
        nullable: true 
    })
    isExpertsCriteries?: boolean;


    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Show Russia map in footer in About page",  
        nullable: true 
    })
    isShowMap?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Show Reglament button on About page",  
        nullable: true 
    })
    isShowReglament?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Show Honnor hall on About page with all Project that have a special label",  
        nullable: true 
    })
    isShowLaureates?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Show Education organizations list on About page",  
        nullable: true 
    })
    isShowSchools?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {   
        description: "Show Tracks list ob About page?",  
        nullable: true 
    })
    isShowTracks?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {   
        description: "User may change card type on Projects page",  
        nullable: true 
    })
    isChangeCardType?: boolean;

    @Column("boolean", {nullable: true, default: true})
    @Field(() => Boolean, {  
        description: "Project participants keep a diary",  
        nullable: true 
    })
    isDiary?: boolean;

    @Column("integer", {nullable: true, default: 3 })
    @Field(() => Int, {   
        description: "Maximal rate Project",  
        nullable: true 
    })
    maxRating?: number;

    @Column({ nullable: true, type: "enum", enum: FESTIVAL_RATING_TYPES, default: FESTIVAL_RATING_TYPES.INDEPENDENT_GRADE })
    @Field(() => FESTIVAL_RATING_TYPES, { 
        description: "Rating type of Festival",  
        nullable: true 
    })
    ratingType?: FESTIVAL_RATING_TYPES; 

    @Column({ nullable: true, type: "enum", enum: DISPLAY_PROJECT_RESULT, default: DISPLAY_PROJECT_RESULT.AVERAGE })
    @Field(() => DISPLAY_PROJECT_RESULT, { 
        description: "Rating type of Festival",  
        nullable: true 
    })
    displayProjectResult?: DISPLAY_PROJECT_RESULT;

    @Column({ nullable: true, type: "enum", enum: CARD_TYPE, default: CARD_TYPE.CARD })
    @Field(() => CARD_TYPE, { 
        description: "Current member's card type",  
        nullable: true 
    })
    memberCardType?: CARD_TYPE;

    @Column("integer", {nullable: true, default: 220})
    @Field(() => Int, {  
        description: "Member card height (only for 'Card' type)",  
        nullable: true 
    })
    memberCardHeight?: number;

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "URL of default Project's thumbnail",  
        nullable: true 
    })
    defaultProjectThumbnail?: string;

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "Web client email after User create new Project",  
        nullable: true 
    })
    newProjectEmail?: string;

    @Column("varchar", {nullable: true})
    @Field(() => String, {  
        description: "Web client message after User create new Project",  
        nullable: true 
    })
    newProjectMessage?: string; 
 

    @Field(() => [PEFestivalExtendedFieldEntity], {nullable: true})
    @OneToMany(() => PEFestivalExtendedFieldEntity, (file:PEFestivalExtendedFieldEntity) => file.festival )
    @ApiProperty({ type: PEFestivalExtendedFieldEntity, isArray:true, example: [], nullable: true })
    extendedFields?: PEFestivalExtendedFieldEntity[];
 
    // extendedFields

    // memberCount

    // countOfResults  

    @OneToOne(() => LandEntity)
    @JoinColumn()
    @HideField()
    land: LandEntity;

}