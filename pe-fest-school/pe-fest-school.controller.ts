import { PEController } from "@common/pe.controller";
import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEFestSchoolService } from "./pe-fest-school.service";

@Controller('pe-fest-schools') 
@ApiTags("pe-fest-schools") 
export class PEFestSchoolController extends PEController {
    constructor( private readonly _peFestSchoolService: PEFestSchoolService ){
        super( _peFestSchoolService );
    }        
}