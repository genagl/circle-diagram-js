import { PERepository } from "@common/repositories";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreatePEFestSchoolDto } from "./dto/create-pe-fest-school.dto";
import { PEFestSchoolEntity } from "./entities/pe-fest-school.entity";

@Injectable()
export class PEFestSchoolService extends PERepository<PEFestSchoolEntity, CreatePEFestSchoolDto> {
    constructor( 
        @InjectRepository(PEFestSchoolEntity)
        private readonly _peFestSchoolRepository: Repository<PEFestSchoolEntity>,
    ) {
        super(_peFestSchoolRepository, "pe-fest-school"); 
    }
}