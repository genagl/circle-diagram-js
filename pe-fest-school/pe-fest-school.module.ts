import { PEFestSchoolService } from './pe-fest-school.service';
import { PEFestSchoolResolver } from './pe-fest-school.resolver';
import { PEFestSchoolController } from './pe-fest-school.controller';
import { PEFestSchoolEntity } from './entities/pe-fest-school.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [
        TypeOrmModule.forFeature([ PEFestSchoolEntity ]),
        LandsModule,
        UsersModule
    ], 
    providers: [PEFestSchoolService, PEFestSchoolResolver, ], 
    controllers: [PEFestSchoolController, ],
    exports: [PEFestSchoolService]
})
export class PeFestSchoolModule {}
