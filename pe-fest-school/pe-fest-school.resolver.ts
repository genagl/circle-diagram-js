import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestSchoolDto } from "./dto/create-pe-fest-school.dto";
import { PEFestSchoolEntity } from "./entities/pe-fest-school.entity";
import { PEFestSchoolService } from "./pe-fest-school.service";


@Resolver(() => PEFestSchoolEntity)
export class PEFestSchoolResolver extends PEResolver(
    PEFestSchoolEntity,
    CreatePEFestSchoolDto, 
    "PEFestSchool"
) {
    constructor( 
        private readonly _service: PEFestSchoolService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}