import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestSchoolInput")
export class CreatePEFestSchoolDto extends CreatePEDto {
    
}