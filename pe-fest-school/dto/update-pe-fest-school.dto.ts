import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestSchoolDto } from './create-pe-fest-school.dto';

@InputType("PEFestSchoolInput")
export class UpdatePEFestSchoolDto extends PartialType(CreatePEFestSchoolDto) {

}