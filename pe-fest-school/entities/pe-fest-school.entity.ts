import { PELandedEntity } from "@common/entities";
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import { PEFestDestrictEntity } from "../../pe-fest-destrict/entities/pe-fest-destrict.entity";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";

@Entity("pe-fest-schools")
@ObjectType("PEFestSchool", { description: 'pe-fest-schools' })
export class PEFestSchoolEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;
 
    @ManyToMany(() => PEFestDestrictEntity, (destrict) => destrict.school)
    @JoinTable()
    @Field({  description: 'Parent PEFestDestricts list', nullable: 'items' })
    destrict?: PEFestDestrictEntity[]; 

    @ManyToMany(() => PEFestProjectEntity, category => category.fmru_school) 
    @Field({  description: 'Child Projects list', nullable: 'items' })
    project?: PEFestProjectEntity[];
}