import { ID } from "@common/interface";
import { LandService } from "@lands/land.service";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { DeepPartial, Repository } from "typeorm";
import { CreatePEFestivalDto } from "./dto/create-pefestival.dto";
import { PEFestivalEntity } from "./entities/pe-festival.entity";
import { IFestival } from './interface'; 
@Injectable()
export class PEFestivalService {
    constructor( 
        @InjectRepository(PEFestivalEntity)
        private readonly _repository: Repository<PEFestivalEntity>, 
        private readonly _landRepository: LandService,
    ) { }

    private async getFestivalByLandId( landId: ID ) : Promise<PEFestivalEntity> {
        let fest = await this._repository
            .createQueryBuilder("festival")
            .leftJoinAndSelect("festival.land", "land")
            .where("festival.landId = :landId", { landId })
            .getOne(); 
        return !fest ? { land:{} } as PEFestivalEntity : fest; 
    }

    // eslint-disable-next-line @typescript-eslint/no-inferrable-types, @typescript-eslint/no-unused-vars
    async findById( id: ID, land_id: number=0 ) { 
        let fests = await this.getFestivalByLandId(id);
        let f = {...fests, ...fests.land};
        delete f.land; 
        return f;
    }
  

    async create(dto: DeepPartial<CreatePEFestivalDto> ) {
        return await this._repository.save(dto);
    }

    async update(id:number, dto: any ) {

        // update Festival
        let item = await this.getFestivalByLandId(id);
        let festData = {}
        Object.keys(dto)
            .filter(key => key as keyof IFestival)
            .forEach(key => {
                festData[key] = dto[key]
            })
        await this._repository.update(id, {...item, ...festData});

        // update Land

        // return
        return await this.findById(id);
    }

    // eslint-disable-next-line @typescript-eslint/no-inferrable-types, @typescript-eslint/no-empty-function, @typescript-eslint/no-unused-vars
    async findAll(options: any, land_id: number=0 ) {
        
    }
}