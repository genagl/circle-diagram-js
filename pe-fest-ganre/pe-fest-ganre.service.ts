import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestGanreEntity } from "./entities/pe-fest-ganre.entity";
import { CreatePEFestGanreDto } from "./dto/create-pe-fest-ganre.dto";

@Injectable()
export class PEFestGanreService extends PERepository<PEFestGanreEntity, CreatePEFestGanreDto> {
    constructor( 
        @InjectRepository(PEFestGanreEntity)
        private readonly _peFestGanreRepository: Repository<PEFestGanreEntity>,
    ) {
        super(_peFestGanreRepository, "pe-fest-ganre"); 
    }
}