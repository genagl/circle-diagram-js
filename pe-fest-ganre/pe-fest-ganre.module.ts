import { PEFestGanreService } from './pe-fest-ganre.service';
import { PEFestGanreResolver } from './pe-fest-ganre.resolver';
import { PEFestGanreController } from './pe-fest-ganre.controller';
import { PEFestGanreEntity } from './entities/pe-fest-ganre.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestGanreEntity ]),
        LandsModule,
        UsersModule], 
    providers: [PEFestGanreService, PEFestGanreResolver,], 
    controllers: [ PEFestGanreController, ] 
})
export class PEFestGanreModule {}
