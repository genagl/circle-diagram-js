import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestGanreDto } from './create-pe-fest-ganre.dto';

@InputType("PEFestGanreInput")
export class UpdatePEFestGanreDto extends PartialType(CreatePEFestGanreDto) {

}