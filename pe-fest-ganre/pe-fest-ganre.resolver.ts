import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestGanreDto } from "./dto/create-pe-fest-ganre.dto";
import { PEFestGanreEntity } from "./entities/pe-fest-ganre.entity";
import { PEFestGanreService } from "./pe-fest-ganre.service";


@Resolver(() => PEFestGanreEntity)
export class PEFestGanreResolver extends PEResolver(
    PEFestGanreEntity,
    CreatePEFestGanreDto, 
    "PEFestGanre"
) {
    constructor( 
        private readonly _service: PEFestGanreService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}