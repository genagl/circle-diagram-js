import { Column, Entity, ManyToMany } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";

@Entity("pe-fest-ganres")
@ObjectType("PEFestGanre", { description: 'pe-fest-ganres' })
export class PEFestGanreEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;

    @Column({ nullable: true, default: "#EEEEEE"}) 
    @ApiProperty({ required: false, description: "Color" })
    @Field(() => String, { nullable: true } )
    color?: string;


    @ManyToMany(() => PEFestProjectEntity, pr => pr.ganre) 
    @Field({  description: 'Child Project list', nullable: 'items' })
    project?: PEFestProjectEntity[];
}