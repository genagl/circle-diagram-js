import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestGanreService } from "./pe-fest-ganre.service";

@Controller('pe-fest-ganres') 
@ApiTags("pe-fest-ganres") 
export class PEFestGanreController extends PEController {
    constructor( private readonly _peFestGanreService: PEFestGanreService ){
        super( _peFestGanreService );
    }        
}