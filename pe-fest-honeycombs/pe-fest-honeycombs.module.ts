import { PEFestHoneycombsService } from './pe-fest-honeycombs.service';
import { PEFestHoneycombsResolver } from './pe-fest-honeycombs.resolver';
import { PEFestHoneycombsController } from './pe-fest-honeycombs.controller';
import { PEFestHoneycombsEntity } from './entities/pe-fest-honeycombs.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestHoneycombsEntity ]),
        LandsModule,
        UsersModule], 
    providers: [PEFestHoneycombsService, PEFestHoneycombsResolver,], 
    controllers: [PEFestHoneycombsController,] ,
    exports:[PEFestHoneycombsService]
})
export class PEFestHoneycombsModule {}
