import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestHoneycombsDto } from './create-pe-fest-honeycombs.dto';

@InputType("PEFestHoneycombsInput")
export class UpdatePEFestHoneycombsDto extends PartialType(CreatePEFestHoneycombsDto) {

}