import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestHoneycombsService } from "./pe-fest-honeycombs.service";

@Controller('pefesthoneycombss') 
@ApiTags("pefesthoneycombss") 
export class PEFestHoneycombsController extends PEController {
    constructor( private readonly _peFestHoneycombsService: PEFestHoneycombsService ){
        super( _peFestHoneycombsService );
    }        
}