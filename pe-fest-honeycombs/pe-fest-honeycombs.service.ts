import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestHoneycombsEntity } from "./entities/pe-fest-honeycombs.entity";
import { CreatePEFestHoneycombsDto } from "./dto/create-pe-fest-honeycombs.dto";

@Injectable()
export class PEFestHoneycombsService extends PERepository<PEFestHoneycombsEntity, CreatePEFestHoneycombsDto> {
    constructor( 
        @InjectRepository(PEFestHoneycombsEntity)
        private readonly _peFestHoneycombsRepository: Repository<PEFestHoneycombsEntity>,
    ) {
        super(_peFestHoneycombsRepository, "pe-fest-honeycombs"); 
    }
}