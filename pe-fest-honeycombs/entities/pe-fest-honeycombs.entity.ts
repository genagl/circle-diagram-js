import { Column, Entity, ManyToMany, ManyToOne } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";

@Entity("pe-fest-honeycombss")
@ObjectType("PEFestHoneycombs", { description: 'pe-fest-honeycombss' })
export class PEFestHoneycombsEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number; 

    @ManyToOne(() => PEFestProjectEntity, category => category.honeycombs)  
    @Field({  description: 'Child Project list', nullable: 'items' })
    project?: PEFestProjectEntity[];

}