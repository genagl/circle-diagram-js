import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestHoneycombsDto } from "./dto/create-pe-fest-honeycombs.dto";
import { PEFestHoneycombsEntity } from "./entities/pe-fest-honeycombs.entity";
import { PEFestHoneycombsService } from "./pe-fest-honeycombs.service";


@Resolver(() => PEFestHoneycombsEntity)
export class PEFestHoneycombsResolver extends PEResolver(
    PEFestHoneycombsEntity,
    CreatePEFestHoneycombsDto, 
    "PEFestHoneycombs"
) {
    constructor( 
        private readonly _service: PEFestHoneycombsService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}