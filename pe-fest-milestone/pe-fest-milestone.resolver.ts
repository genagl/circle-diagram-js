import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestMilestoneDto } from "./dto/create-pe-fest-milestone.dto";
import { PEFestMilestoneEntity } from "./entities/pe-fest-milestone.entity";
import { PEFestMilestoneService } from "./pe-fest-milestone.service";


@Resolver(() => PEFestMilestoneEntity)
export class PEFestMilestoneResolver extends PEResolver(
    PEFestMilestoneEntity,
    CreatePEFestMilestoneDto, 
    "PEFestMilestone"
) {
    constructor( 
        private readonly _service: PEFestMilestoneService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}