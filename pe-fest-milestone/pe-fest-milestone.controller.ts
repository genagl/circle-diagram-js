import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestMilestoneService } from "./pe-fest-milestone.service";

@Controller('pe-fest-milestones') 
@ApiTags("pe-fest-milestones") 
export class PEFestMilestoneController extends PEController {
    constructor( private readonly _peFestMilestoneService: PEFestMilestoneService ){
        super( _peFestMilestoneService );
    }        
}