import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestMilestoneDto } from './create-pe-fest-milestone.dto';

@InputType("PEFestMilestoneInput")
export class UpdatePEFestMilestoneDto extends PartialType(CreatePEFestMilestoneDto) {

}