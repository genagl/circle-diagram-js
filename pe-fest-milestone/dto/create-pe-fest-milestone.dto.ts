import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestMilestoneInput")
export class CreatePEFestMilestoneDto extends CreatePEDto {
    
}