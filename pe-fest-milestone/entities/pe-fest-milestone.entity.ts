import { Column, Entity, OneToMany } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { PEFestCriteryEntity } from "../../pe-fest-critery/entities/pe-fest-critery.entity";

@Entity("pe-fest-milestones")
@ObjectType("PEFestMilestone", { description: 'pe-fest-milestones' })
export class PEFestMilestoneEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;

    @Column("boolean", { nullable: true}) 
    @ApiProperty({ required: false, description: "is attachments required?" })
    @Field(() => Boolean, { nullable: true } )
    is_attachments_required?: boolean;

    @Column("boolean", { nullable: true}) 
    @ApiProperty({ required: false, description: "is Project owners may rating its?" })
    @Field(() => Boolean, { nullable: true } )
    is_auto_rait?: boolean;
    
    @Field(() => [PEFestCriteryEntity], {nullable: true})
    // @ManyToOne(() => PEFestCriteryEntity, (cr:PEFestCriteryEntity) => cr.milestone )
    @ApiProperty({ type: PEFestCriteryEntity, isArray:true, example: [ ], nullable: true })
    criteries?: PEFestCriteryEntity[];

}