import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestMilestoneEntity } from "./entities/pe-fest-milestone.entity";
import { CreatePEFestMilestoneDto } from "./dto/create-pe-fest-milestone.dto";

@Injectable()
export class PEFestMilestoneService extends PERepository<PEFestMilestoneEntity, CreatePEFestMilestoneDto> {
    constructor( 
        @InjectRepository(PEFestMilestoneEntity)
        private readonly _peFestMilestoneRepository: Repository<PEFestMilestoneEntity>,
    ) {
        super(_peFestMilestoneRepository, "pe-fest-milestone"); 
    }
}