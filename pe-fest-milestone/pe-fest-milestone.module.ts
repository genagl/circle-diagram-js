import { PEFestMilestoneService } from './pe-fest-milestone.service';
import { PEFestMilestoneResolver } from './pe-fest-milestone.resolver';
import { PEFestMilestoneController } from './pe-fest-milestone.controller';
import { PEFestMilestoneEntity } from './entities/pe-fest-milestone.entity';
 import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestMilestoneEntity ]),
        LandsModule,
        UsersModule], 
    providers: [PEFestMilestoneService, PEFestMilestoneResolver,], 
    controllers: [ PEFestMilestoneController,],
    exports: [PEFestMilestoneService]
})
export class PEFestMilestoneModule {}
