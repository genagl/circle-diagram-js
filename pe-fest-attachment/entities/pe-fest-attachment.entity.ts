import { Column, Entity } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql'; 
import { PELandedEntity } from "@src/common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { SCALAR_TYPES } from "@common/interface/scalar-types.enum";

@Entity("pe-fest-attachments")
@ObjectType("PEFestAttachment", { description: 'pe fest attachments' })
export class PEFestAttachmentEntity extends PELandedEntity {
     
 
    @Field(() => String, { nullable: true,  description: "Uniq identifier." })
    ID: number;

    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Data of attachment(link or content)", nullable: true }) 
    @ApiProperty({ nullable: true  })
    public data?: string;

    @Column("enum", {enum: SCALAR_TYPES,  nullable: true })  
    @Field(() => SCALAR_TYPES, { description: "Type", nullable: true }) 
    @ApiProperty({ nullable: true  })
    public type?: SCALAR_TYPES;

    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Description", nullable: true }) 
    @ApiProperty({ nullable: true  })
    public descr?: string;

    @Column("integer", { nullable: true })  
    @Field(() => String, { description: "Parent Project ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    public member_id?: number;

    @Column("integer", { nullable: true })  
    @Field(() => String, { description: "Parent Milestone ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    public milestone_id?: number;

    @Column("varchar", { nullable: true })  
    @Field(() => String, { description: "Unix time of creation", nullable: true }) 
    @ApiProperty({ nullable: true  }) 
    public unixtime?: string;
}