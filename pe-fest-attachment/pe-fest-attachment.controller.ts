import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestAttachmentService } from "./pe-fest-attachment.service";

@Controller('pe-fest-attachments') 
@ApiTags("pe-fest-attachments") 
export class PEFestAttachmentController extends PEController {
    constructor( private readonly _peFestAttachmentService: PEFestAttachmentService ){
        super( _peFestAttachmentService );
    }        
}