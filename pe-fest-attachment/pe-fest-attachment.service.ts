import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestAttachmentEntity } from "./entities/pe-fest-attachment.entity";
import { CreatePEFestAttachmentDto } from "./dto/create-pe-fest-attachment.dto";

@Injectable()
export class PEFestAttachmentService extends PERepository<PEFestAttachmentEntity, CreatePEFestAttachmentDto> {
    constructor( 
        @InjectRepository(PEFestAttachmentEntity)
        private readonly _peFestAttachmentRepository: Repository<PEFestAttachmentEntity>,
    ) {
        super(_peFestAttachmentRepository, "pe-fest-attachment"); 
    }
 
}