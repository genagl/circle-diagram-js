import { PEFestAttachmentService } from './pe-fest-attachment.service';
import { PEFestAttachmentResolver } from './pe-fest-attachment.resolver';
import { PEFestAttachmentController } from './pe-fest-attachment.controller';
import { PEFestAttachmentEntity } from './entities/pe-fest-attachment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { LandsModule } from '@lands/lands.module'; 

import { Module } from '@nestjs/common';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestAttachmentEntity ]),
        LandsModule,
        UsersModule], 
    providers: [PEFestAttachmentService, PEFestAttachmentResolver,], 
    controllers: [ PEFestAttachmentController, ],
    exports: [PEFestAttachmentService]
})
export class PEFestAttachmentModule {}
