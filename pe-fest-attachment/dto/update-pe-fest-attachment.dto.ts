import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestAttachmentDto } from './create-pe-fest-attachment.dto';

@InputType("PEFestAttachmentInput")
export class UpdatePEFestAttachmentDto extends PartialType(CreatePEFestAttachmentDto) {

}