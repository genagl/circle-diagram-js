
import { ID as _ID } from "@common/scalars/";
import { Field, ID, InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";
import { SCALAR_TYPES } from "@common/interface/scalar-types.enum";

@InputType("PEFestAttachmentInput")
export class CreatePEFestAttachmentDto extends CreatePEDto {
 
    @Field(() => String, {  
        description: "Data",  
        nullable: true 
    })
    data?: string

    @Field(() => SCALAR_TYPES, {  
        description: "Type ()",  
        nullable: true 
    })
    type?: SCALAR_TYPES;

    @Field(() => String, {  
        description: "Description",  
        nullable: true 
    })
    descr?: string;

    @Field(() => ID, {  
        description: "Project ID",  
        nullable: true 
    })
    member_id?: number;

    @Field(() => ID, {  
        description: "Milestone ID",  
        nullable: true 
    })
    milestone_id?: number;

    @Field(() => String, {  
        description: "Unix Time by string",  
        nullable: true 
    })
    unixtime?: string;

}