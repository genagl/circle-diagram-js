import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { PEResolver } from "@src/common/pe.resolver";
import { UsersService } from "@users/users.service"; 
import { PEFestAttachmentEntity } from "./entities/pe-fest-attachment.entity";
import { PEFestAttachmentService } from "./pe-fest-attachment.service";
import { CreatePEFestAttachmentDto } from "./dto/create-pe-fest-attachment.dto";


@Resolver(() => PEFestAttachmentEntity)
export class PEFestAttachmentResolver extends PEResolver(
    PEFestAttachmentEntity,
    CreatePEFestAttachmentDto, 
    "PEFestAttachment"
) {
    constructor( 
        private readonly _service: PEFestAttachmentService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}