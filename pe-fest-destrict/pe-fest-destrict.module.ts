import { LandsModule } from '@lands/lands.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { PEFestDestrictEntity } from './entities/pe-fest-destrict.entity';
import { PEFestDestrictController } from './pe-fest-destrict.controller';
import { PEFestDestrictResolver } from './pe-fest-destrict.resolver';
import { PEFestDestrictService } from './pe-fest-destrict.service';
import { PeFestSchoolModule } from '../pe-fest-school/pe-fest-school.module';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestDestrictEntity ]),
        LandsModule,
        UsersModule,
        PeFestSchoolModule
    ], 
    providers: [PEFestDestrictService, PEFestDestrictResolver, ], 
    controllers: [PEFestDestrictController, ],
    exports: [PEFestDestrictService]
})
export class PeFestDestrictModule {}
