import { Column, Entity, ManyToMany, ManyToOne } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";
import { PEFestSchoolEntity } from "../../pe-fest-school/entities/pe-fest-school.entity";

@Entity("pe-fest-destricts")
@ObjectType("PEFestDestrict", { description: 'pe-fest-destricts' })
export class PEFestDestrictEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;
 
    @ManyToMany(() => PEFestSchoolEntity, (school) => school.destrict) 
    @Field({  description: 'Child PEFestSchools list', nullable: 'items' })
    school?: PEFestSchoolEntity[];

    @ManyToOne(() => PEFestProjectEntity, category => category.fmru_destrict) 
    @Field({  description: 'Child Projects list', nullable: 'items' })
    project?: PEFestProjectEntity[];
}