import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestDestrictService } from "./pe-fest-destrict.service";

@Controller('pe-fest-destricts') 
@ApiTags("pe-fest-destricts") 
export class PEFestDestrictController extends PEController {
    constructor( private readonly _pePestDestrictService: PEFestDestrictService ){
        super( _pePestDestrictService );
    }        
}