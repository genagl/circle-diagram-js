import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestDestrictDto } from './create-pe-fest-destrict.dto';

@InputType("PEFestDestrictInput")
export class UpdatePEFestDestrictDto extends PartialType(CreatePEFestDestrictDto) {

}