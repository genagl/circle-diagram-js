import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestDestrictInput")
export class CreatePEFestDestrictDto extends CreatePEDto {
    
}