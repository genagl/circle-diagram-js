import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestDestrictDto } from "./dto/create-pe-fest-destrict.dto";
import { PEFestDestrictEntity } from "./entities/pe-fest-destrict.entity";
import { PEFestDestrictService } from "./pe-fest-destrict.service";


@Resolver(() => PEFestDestrictEntity)
export class PEFestDestrictResolver extends PEResolver(
    PEFestDestrictEntity,
    CreatePEFestDestrictDto, 
    "PEFestDestrict"
) {
    constructor( 
        private readonly _service: PEFestDestrictService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}