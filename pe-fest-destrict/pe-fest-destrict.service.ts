import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestDestrictEntity } from "./entities/pe-fest-destrict.entity";
import { CreatePEFestDestrictDto } from "./dto/create-pe-fest-destrict.dto";

@Injectable()
export class PEFestDestrictService extends PERepository<PEFestDestrictEntity, CreatePEFestDestrictDto> {
    constructor( 
        @InjectRepository(PEFestDestrictEntity)
        private readonly _peFestDestrictRepository: Repository<PEFestDestrictEntity>,
    ) {
        super(_peFestDestrictRepository, "pe-fest-destrict"); 
    }
}