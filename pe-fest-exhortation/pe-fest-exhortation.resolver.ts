import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestExhortationDto } from "./dto/create-pe-fest-exhortation.dto";
import { PEFestExhortationEntity } from "./entities/pe-fest-exhortation.entity";
import { PEFestExhortationService } from "./pe-fest-exhortation.service";


@Resolver(() => PEFestExhortationEntity)
export class PEFestExhortationResolver extends PEResolver(
    PEFestExhortationEntity,
    CreatePEFestExhortationDto, 
    "PEFestExhortation"
) {
    constructor( 
        private readonly _service: PEFestExhortationService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}