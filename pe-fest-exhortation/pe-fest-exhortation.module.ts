import { LandsModule } from '@lands/lands.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { PEFestExhortationEntity } from './entities/pe-fest-exhortation.entity';
import { PEFestExhortationController } from './pe-fest-exhortation.controller';
import { PEFestExhortationResolver } from './pe-fest-exhortation.resolver';
import { PEFestExhortationService } from './pe-fest-exhortation.service';

@Module({ 
    imports: [
        TypeOrmModule.forFeature([ PEFestExhortationEntity ]),
        LandsModule,
        UsersModule 
    ], 
    providers: [PEFestExhortationService, PEFestExhortationResolver, ], 
    controllers: [PEFestExhortationController,] ,
    exports:[PEFestExhortationService]
})
export class PeFestExhortationModule {}
