import { Column, Entity, JoinColumn, ManyToOne } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { PEFestProjectEntity } from "../../pe-fest-project/entities/pe-fest-project.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity("pe-fest-exhortations")
@ObjectType("PEFestExhortation", { description: 'pe-fest-exhortations' })
export class PEFestExhortationEntity extends PELandedEntity {
    
  @Field(() => PEFestProjectEntity )
  @ManyToOne(() => PEFestProjectEntity, (pr: PEFestProjectEntity) => pr.exhortations)
  @JoinColumn()
  project: PEFestProjectEntity; 
 
  @Column({ nullable: true}) 
  @ApiProperty({ example: "", required: false, description: 'Project ID' })
  @Field(() => Int, { nullable: true } )
  memberId?: number; 

  @Column({ nullable: true }) 
  @Field({  nullable: true, description: "Date" })  
  @ApiProperty({ name: "Date"}) 
  unixtime?: Date | null;

  @Column("varchar", { nullable: true }) 
  @Field(() => String, {  nullable: true, description: "descr" })  
  @ApiProperty({ name: "descr"}) 
  descr?: string | null;
}