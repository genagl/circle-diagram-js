import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestExhortationService } from "./pe-fest-exhortation.service";

@Controller('pefestexhortations') 
@ApiTags("pefestexhortations") 
export class PEFestExhortationController extends PEController {
    constructor( private readonly _peFestExhortationService: PEFestExhortationService ){
        super( _peFestExhortationService );
    }        
}