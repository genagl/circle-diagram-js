import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestExhortationDto } from './create-pe-fest-exhortation.dto';

@InputType("PEFestExhortationInput")
export class UpdatePEFestExhortationDto extends PartialType(CreatePEFestExhortationDto) {

}