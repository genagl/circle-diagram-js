import { Field, InputType, Int } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestExhortationInput")
export class CreatePEFestExhortationDto extends CreatePEDto {
    
    @Field(() => String, {  
        description: "Description",  
        nullable: true 
    })
    descr? : string;

    @Field(() => Int, {  
        description: "Project ID",  
        nullable: true 
    })
    memberId?: number;

    @Field(() => Int, {  
        description: "Expert ID",  
        nullable: true 
    })
    expertId?: number;
}