import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestExhortationEntity } from "./entities/pe-fest-exhortation.entity";
import { CreatePEFestExhortationDto } from "./dto/create-pe-fest-exhortation.dto";

@Injectable()
export class PEFestExhortationService extends PERepository<PEFestExhortationEntity, CreatePEFestExhortationDto> {
    constructor( 
        @InjectRepository(PEFestExhortationEntity)
        private readonly _peFestExhortationRepository: Repository<PEFestExhortationEntity>,
    ) {
        super(_peFestExhortationRepository, "pe-fest-exhortation"); 
    }
}