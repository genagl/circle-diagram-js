import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestExhortationDto } from './create-pefestexhortation.dto';

@InputType("PEFestExhortationInput")
export class UpdatePEFestExhortationDto extends PartialType(CreatePEFestExhortationDto) {

}