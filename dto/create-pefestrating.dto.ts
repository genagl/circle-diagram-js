import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestRatingInput")
export class CreatePEFestRatingDto extends CreatePEDto {
    
}