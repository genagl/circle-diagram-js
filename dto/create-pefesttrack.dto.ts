import { Field, InputType, Int } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";
import { ApiProperty } from "@nestjs/swagger";

@InputType("PEFestTrackInput")
export class CreatePEFestTrackDto extends CreatePEDto {
    
    @Field({  description: 'Is track closed for join?', defaultValue: false })
    @ApiProperty({example: false, required: false, description: 'Is track closed for join?' })
    is_closed?: boolean;

    @ApiProperty({ example: "", required: false, description: "Track moderator's ID" })
    @Field(() => Int, { nullable: true } )
    moderator_id?: number;  
 
    @ApiProperty({ example: "", required: false, description: "Track moderator assistant's ID" })
    @Field(() => Int, { nullable: true } )
    moderator2_id?: number; 
    
    @Field(() => String, { description: "Youtube describe's ID", nullable: true }) 
    @ApiProperty({ nullable: true  })
    video_id?: string;
    
    @Field(() => String, { description: "illustration", nullable: true }) 
    @ApiProperty({ nullable: true  })
    illustration?: string;
}