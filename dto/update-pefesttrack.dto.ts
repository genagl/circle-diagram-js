import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestTrackDto } from './create-pefesttrack.dto';

@InputType("PEFestTrackInput")
export class UpdatePEFestTrackDto extends PartialType(CreatePEFestTrackDto) {

}