import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestMilestoneDto } from './create-pefestmilestone.dto';

@InputType("PEFestMilestoneInput")
export class UpdatePEFestMilestoneDto extends PartialType(CreatePEFestMilestoneDto) {

}