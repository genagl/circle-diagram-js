import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestivalDto } from './create-pefestival.dto';

@InputType("PEFestivalInput")
export class UpdatePEFestivalDto extends PartialType(CreatePEFestivalDto) {

}