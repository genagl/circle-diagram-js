import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestRatingDto } from './create-pefestrating.dto';

@InputType("PEFestRatingInput")
export class UpdatePEFestRatingDto extends PartialType(CreatePEFestRatingDto) {

}