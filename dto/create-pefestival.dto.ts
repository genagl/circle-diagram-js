import { Field, ID, InputType } from "@nestjs/graphql"; 

@InputType("PEFestivalInput")
export class CreatePEFestivalDto {
  
    @Field(() => Boolean, { 
        defaultValue: false,  
        description: "Is owner of Project may rate himself?",  
        nullable: true 
    })
    isOwnerRate?: boolean;

    @Field(() => Boolean, { 
        defaultValue: false,  
        description: "Experts may create new criteries",  
        nullable: true 
    })
    isExpertsCriteries?: boolean;

    @Field(() => Boolean, { 
        defaultValue: true,  
        description: "Show Education organizations list on About page",  
        nullable: true 
    })
    isShowSchools?: boolean;

}