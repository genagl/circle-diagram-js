import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestSchoolDto } from './create-pefestschool.dto';

@InputType("PEFestSchoolInput")
export class UpdatePEFestSchoolDto extends PartialType(CreatePEFestSchoolDto) {

}