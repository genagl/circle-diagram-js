import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestHoneycombsInput")
export class CreatePEFestHoneycombsDto extends CreatePEDto {
    
}