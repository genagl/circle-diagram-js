import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestHoneycombsDto } from './create-pefesthoneycombs.dto';

@InputType("PEFestHoneycombsInput")
export class UpdatePEFestHoneycombsDto extends PartialType(CreatePEFestHoneycombsDto) {

}