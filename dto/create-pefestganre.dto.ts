import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestGanreInput")
export class CreatePEFestGanreDto extends CreatePEDto {
    
}