import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestGanreDto } from './create-pefestganre.dto';

@InputType("PEFestGanreInput")
export class UpdatePEFestGanreDto extends PartialType(CreatePEFestGanreDto) {

}