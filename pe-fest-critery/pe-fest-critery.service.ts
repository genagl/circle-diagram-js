import { PERepository } from "@common/repositories";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreatePEFestCriteryDto } from "./dto/create-pe-fest-critery.dto";
import { PEFestCriteryEntity } from "./entities/pe-fest-critery.entity";

@Injectable()
export class PEFestCriteryService extends PERepository<PEFestCriteryEntity, CreatePEFestCriteryDto> {
    constructor( 
        @InjectRepository(PEFestCriteryEntity)
        private readonly _peFestCriteryRepository: Repository<PEFestCriteryEntity>,
    ) {
        super(_peFestCriteryRepository, "pe-fest-critery"); 
    } 
    
}