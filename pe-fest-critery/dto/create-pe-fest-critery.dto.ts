import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestCriteryInput")
export class CreatePEFestCriteryDto extends CreatePEDto {
    
}