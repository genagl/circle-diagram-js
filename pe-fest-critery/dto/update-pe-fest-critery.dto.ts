import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestCriteryDto } from './create-pe-fest-critery.dto';

@InputType("PEFestCriteryInput")
export class UpdatePEFestCriteryDto extends PartialType(CreatePEFestCriteryDto) {

}