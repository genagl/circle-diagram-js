import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestCriteryDto } from "./dto/create-pe-fest-critery.dto";
import { PEFestCriteryEntity } from "./entities/pe-fest-critery.entity";
import { PEFestCriteryService } from "./pe-fest-critery.service";
import { PEFestMilestoneService } from "../pe-fest-milestone/pe-fest-milestone.service";
import { PEFestCategoryService } from "../pefest-category/pefest-category.service";


@Resolver(() => PEFestCriteryEntity)
export class PEFestCriteryResolver extends PEResolver(
    PEFestCriteryEntity,
    CreatePEFestCriteryDto, 
    "PEFestCritery"
) {
    constructor( 
        private readonly _service: PEFestCriteryService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
        private readonly _milestone: PEFestMilestoneService,
        private readonly _category: PEFestCategoryService,
    ) {
        super( _service, _lands, _users ); 
    }
        
    @ResolveField("milestone", () => PEFestCriteryEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async milestone(@Parent() project:  PEFestCriteryEntity ) {
        const milestone_id: number = project.milestone_id;  
        return milestone_id 
            ? 
            this._milestone.findById( milestone_id )
            : 
            null;
    }
    
        
    @ResolveField("category", () => PEFestCriteryEntity, { complexity: 4, nullable: true, description: "Tutor." })
    async category(@Parent() project:  PEFestCriteryEntity ) {
        const category_id: number = project.category_id;  
        return category_id 
            ? 
            this._category.findById( category_id )
            : 
            null;
    }
}