import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { PEController } from "@common/pe.controller";
import { PEFestCriteryService } from "./pe-fest-critery.service";

@Controller('pe-fest-criteries') 
@ApiTags("pe-fest-criteries") 
export class PEFestCriteryController extends PEController {
    constructor( private readonly _peFestCriteryService: PEFestCriteryService ){
        super( _peFestCriteryService );
    }        
}