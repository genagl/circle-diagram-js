import { PELandedEntity } from "@common/entities";
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ApiProperty } from "@nestjs/swagger";
import { UserEntity } from "@users/entities";
import { Column, Entity } from "typeorm";
import { PEFestMilestoneEntity } from "../../pe-fest-milestone/entities/pe-fest-milestone.entity";
import { PEFestCategoryEntity } from "../../pefest-category/entities/pe-fest-category.entity";

@Entity("pe-fest-criteries")
@ObjectType("PEFestCritery", { description: 'pe-fest-criteries' })
export class PEFestCriteryEntity extends PELandedEntity { 
    
    @Column("integer", {nullable: true})
    @Field(() => Int, {  
        description: "max rating",  
        nullable: true 
    })
    max_rating?: number; 
       
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Parent PEFestMilestone ID' })
    @Field(() => Int, { nullable: true } )
    milestone_id?: number;

    @Field(() => PEFestMilestoneEntity, {nullable: true} ) 
    milestone: PEFestMilestoneEntity; 
    
    @Field(() => UserEntity, { description: 'Expert that created.' }) 
    expert?: UserEntity;  
    
    @Field(() => PEFestCategoryEntity, {nullable: true} ) 
    category: PEFestCategoryEntity;   
       
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: 'Parent PEFestCategory ID' })
    @Field(() => Int, { nullable: true } )
    category_id?: number;
}