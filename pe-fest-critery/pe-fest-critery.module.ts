import { LandsModule } from '@lands/lands.module';
import { PEfestCategoryModule } from '@modules/fest/pefest-category/pefest-category.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '@users/users.module';
import { PEFestMilestoneModule } from '../pe-fest-milestone/pe-fest-milestone.module';
import { PEFestCriteryEntity } from './entities/pe-fest-critery.entity';
import { PEFestCriteryController } from './pe-fest-critery.controller';
import { PEFestCriteryResolver } from './pe-fest-critery.resolver';
import { PEFestCriteryService } from './pe-fest-critery.service';

@Module({ 
    imports: [TypeOrmModule.forFeature([ PEFestCriteryEntity ]),
        LandsModule,
        UsersModule,
        PEfestCategoryModule,
        PEFestMilestoneModule
    ], 
    providers: [PEFestCriteryService, PEFestCriteryResolver, ], 
    controllers: [PEFestCriteryController, ],
    exports:[ PEFestCriteryService ]
})
export class PEFestCriteryModule {}
