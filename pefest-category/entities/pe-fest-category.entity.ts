import { Column, Entity, OneToMany } from "typeorm"; 
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { PELandedEntity } from "@common/entities";
import { ApiProperty } from "@nestjs/swagger";  
import { DISPLAY_PROJECT_RESULT, RATING_TYPES } from "../../interface"; 
import { PEFestCriteryEntity } from "../../pe-fest-critery/entities/pe-fest-critery.entity";

@Entity("pe-fest-categories")
@ObjectType("PEFestCategory", { description: 'pe-fest-categories' })
export class PEFestCategoryEntity extends PELandedEntity {
    
    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "Count of projects or over child Resources" })
    @Field(() => Int, { nullable: true } )
    count?: number;

    @Column({ nullable: true}) 
    @ApiProperty({ example: "", required: false, description: "max rating" })
    @Field(() => Int, { nullable: true } )
    max_rating?: number;

    @Column({ nullable: true, default: "#EEEEEE"}) 
    @ApiProperty({ required: false, description: "Color" })
    @Field(() => String, { nullable: true } )
    color?: string;

    @Column({ nullable: true, type: "enum", enum: DISPLAY_PROJECT_RESULT, default: DISPLAY_PROJECT_RESULT.AVERAGE }) 
    @ApiProperty({ required: false, description: "display rating type" })
    @Field(() => DISPLAY_PROJECT_RESULT, { nullable: true } )
    display_rating_type?: DISPLAY_PROJECT_RESULT;

    @Column({ nullable: true, type: "enum", enum: RATING_TYPES, default: RATING_TYPES.INDEPENDENT_GRADE }) 
    @ApiProperty({ required: false, description: "display rating type" })
    @Field(() => RATING_TYPES, { nullable: true } )
    rating_type?: RATING_TYPES;
    
    @Field(() => [PEFestCriteryEntity], {nullable: true})
    // @OneToMany(() => PEFestCriteryEntity, (cr:PEFestCriteryEntity) => cr.category )
    @ApiProperty({ type: PEFestCriteryEntity, isArray:true, example: [ ], nullable: true })
    criteries?: PEFestCriteryEntity[];

}