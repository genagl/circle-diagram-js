import { PartialType } from '@nestjs/swagger';
import { InputType } from "@nestjs/graphql";
import { CreatePEFestCategoryDto } from './create-pefestcategory.dto';

@InputType("PEFestCategoryInput")
export class UpdatePEFestCategoryDto extends PartialType(CreatePEFestCategoryDto) {

}