import { InputType } from "@nestjs/graphql";
import { CreatePEDto } from "@common/dto/create-pe.dto";

@InputType("PEFestCategoryInput")
export class CreatePEFestCategoryDto extends CreatePEDto {
    
}