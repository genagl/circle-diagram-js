import { Module } from '@nestjs/common';
import { PEFestCategoryService } from './pefest-category.service';
import { PEFestCategoryResolver } from './pefest-category.resolver'; 
import { TypeOrmModule } from '@nestjs/typeorm';
import { PEFestCategoryEntity } from './entities/pe-fest-category.entity';
import { LandsModule } from '@lands/lands.module';
import { UsersModule } from '@users/users.module';

@Module({
  providers: [PEFestCategoryResolver, PEFestCategoryService], 
  exports: [PEFestCategoryService],
  imports: [
    TypeOrmModule.forFeature([ PEFestCategoryEntity ]),
        LandsModule,
        UsersModule,
  ]
})
export class PEfestCategoryModule {}
