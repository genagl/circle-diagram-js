import { PEResolver } from "@common/pe.resolver";
import { LandService } from "@lands/land.service";
import { Resolver } from "@nestjs/graphql";
import { UsersService } from "@users/users.service";
import { CreatePEFestCategoryDto } from "./dto/create-pefestcategory.dto";
import { PEFestCategoryEntity } from "./entities/pe-fest-category.entity";
import { PEFestCategoryService } from "./pefest-category.service";


@Resolver(() => PEFestCategoryEntity)
export class PEFestCategoryResolver extends PEResolver(
    PEFestCategoryEntity,
    CreatePEFestCategoryDto, 
    "PEFestCategory"
) {
    constructor( 
        private readonly _service: PEFestCategoryService,
        private readonly _lands: LandService,
        private readonly _users: UsersService,
    ) {
        super( _service, _lands, _users ); 
    }
}