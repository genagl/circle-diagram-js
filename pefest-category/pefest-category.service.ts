import { Injectable } from "@nestjs/common";  
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm"; 
import { PERepository } from "@common/repositories";
import { PEFestCategoryEntity } from "./entities/pe-fest-category.entity";
import { CreatePEFestCategoryDto } from "./dto/create-pefestcategory.dto"; 

@Injectable()
export class PEFestCategoryService extends PERepository<PEFestCategoryEntity, CreatePEFestCategoryDto> {
    constructor( 
        @InjectRepository(PEFestCategoryEntity)
        private readonly _peFestCategoryRepository: Repository<PEFestCategoryEntity>,
    ) {
        super(_peFestCategoryRepository, "pe-fest-category"); 
    }
    
}